
var webpack = require('webpack');

module.exports = {
	entry: './client',
	output: {
		path: __dirname + '/public/js',
		filename: 'client.js'
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel'
				// query: {
				// 	optional: ['optimisation.react.constantElements', 'optimisation.react.inlineElements']
				// }
			}
		]
	}
	// plugins: [
	// 	new webpack.optimize.DedupePlugin()
	// ]
};
