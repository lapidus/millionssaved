
import keystone from 'keystone';

var Types = keystone.Field.Types;
 
var Post = new keystone.List('Post', {
	autokey: { path: 'slug', from: 'title', unique: true },
	map: { name: 'title' },
	defaultSort: '-createdAt'
});
 
Post.add({
	title: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
	author: { type: String },
	createdAt: { type: Date, default: Date.now, hidden: true },
	publishedAt: Date,
	content: {
		brief: { type: Types.Html, wysiwyg: true, height: 150 },
		extended: { type: Types.Html, wysiwyg: true, height: 400 }
	}
});
 
Post.defaultColumns = 'title, state|20%, author, publishedAt|15%';

// Post.register();
