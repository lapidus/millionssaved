require('dotenv').load();
require('babel/register');

var keystone = require('keystone');

keystone.init({

  'name': 'CGDMS',
  'brand': 'CGDMS',

  'less': 'public',
  'static': 'public',
  'static options': {
    maxAge: '1d'
  },
  'favicon': 'public/favicon.ico',
  'views': 'templates/views',
  'view engine': 'jade',

  'compress': true,

  'auto update': true,
  'session': true,
  'auth': true,
  'user model': 'User',
  'cookie secret': 'lQ&amp;]u)ZT-9,cO@iT2T9t4N!&amp;I0{~$JB0[ztU#:(Fg$kkRKxUK!}_iH{27UZd2&amp;(3',

  'mongo': process.env.MONGO_URI || ''
});

keystone.import('models');

keystone.set('locals', {
  _: require('underscore'),
  env: keystone.get('env'),
  utils: keystone.utils,
  editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));

keystone.set('signin redirect', function(user, req, res) {
  var url = (user.isAdmin) ? '/keystone' : '/';
  res.redirect(url);
});

keystone.set('nav', {
  'users': 'users',
  'content': ['case-studies', 'media-items']
});

keystone.start();
