
import React  from 'react';
import { Route, IndexRoute } from 'react-router';

import App       from './components/App';
import Home      from './components/Home';
import About     from './components/About';
import Media     from './components/Media';
import Findings  from './components/Findings';
import FAQ       from './components/FAQ';
import PostList  from './components/PostList';
import CaseStudiesList  from './components/CaseStudiesList';
import CaseStudy from './components/CaseStudy';

import Background from './components/about/Background';
import Cases from './components/about/Cases';
import Team from './components/about/Team';

// const routes = (
// 	<Route path="/" component={App}>
// 		<IndexRoute component={Home}></IndexRoute>
// 		<Route path="about" component={About} />
// 		<Route path="findings" component={Findings} />
// 		<Route path="frequently-asked-questions" component={FAQ} />
// 		<Route path="case-studies" component={CaseStudiesList} />
// 		<Route path="case-studies/:caseId" component={CaseStudy} />
// 	</Route>
// );

const routes = (
	<Route path="/" component={App}>
		<IndexRoute component={Home}></IndexRoute>
		<Route path='about' component={About}>
			<IndexRoute component={Background} />
			<Route path="cases" component={Cases} />
			<Route path="team" component={Team} />
		</Route>
		<Route path="findings" component={Findings} />
		<Route path="frequently-asked-questions" component={FAQ} />
		<Route path="media" component={Media} />
		<Route path="case-studies/:caseId" component={CaseStudy} />
	</Route>
);

export default routes;
