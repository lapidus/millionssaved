
import React from 'react';
import { Link } from 'react-router';

import { connect } from 'react-redux';
import {fetchMediaItemsIfNeeded} from '../actions/mediaItemActions';

class Media extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}

  render() {
    return (
      <div>
        <header className='banner banner--page'>
          <img src='/img/about/media-cover.jpg' />
          <div className='banner-overlay text--center'>
            <div className='container--narrow y-center clearfix px1 px-sm-2'>
              <h1 className='element--0'>
                <span className='subhead text--yellow text--uppercase'>Millions Saved</span><br /><hr className='divider--sm' />
                <span className='display--4'>Media</span>
              </h1>
              <p className='subhead visible--sm element--1 text--yellow text--uppercase'>Millions Saved in the Press</p>
            </div>
          </div>
          <div className='img-source'>
            <a href='#' target='_blank'>Photographer — flickr account...</a>
          </div>
        </header>

        <section className='bg--grey pb3'>

          <div className='container--narrow text--center px1 px-sm-2 py3'>
            <p className='lead'>The description for the media page can be kept fairly short. Three lines would be nice, but of course this space is flexible. The only thing is the sense that there is some kind of introduction for this page.</p>
          </div>

          <div className='container--narrow'>
            <div className='media-item-grid col-sm-12 clearfix'>
              {this.props.mediaItems.map((item, i) => {
                return <MediaItem data={item} key={i} />;
              })}
            </div>
          </div>
        </section>

      </div>
    );
  }
}


class MediaItem extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {

    var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var publishedAt = new Date(this.props.data.publishedAt);
    var sourcePublished = new Date(this.props.data.source.publishedAt);

    return (
      <div className='media-item-grid-item col col-sm-6 px1 px-sm-2 py2'>
        <div className='bg--white px2 py2'>
          <p>{ `${monthNames[publishedAt.getMonth()]} ${publishedAt.getDate()}, ${publishedAt.getFullYear()}` }</p>

          <h2>{this.props.data.title}</h2>
          <p>{this.props.data.intro}</p>

          <p className='text--muted'>Published in <a href={this.props.data.source.url}>{this.props.data.source.name}</a> { `${monthNames[sourcePublished.getMonth()]} ${sourcePublished.getDate()}, ${sourcePublished.getFullYear()}` }</p>

          <a href={this.props.data.source.url} target='_blank' className='btn bg--yellow py05 px1'>Visit Source</a>

        </div>
      </div>
    );
  }
}

Media.needs = [ fetchMediaItemsIfNeeded ];

function mapStateToProps(state) {
  return {
    mediaItems: state.mediaItemsReducer.mediaItems,
    dispatch: state.mediaItemsReducer.dispatch
  };
}

export default connect(mapStateToProps)(Media);
