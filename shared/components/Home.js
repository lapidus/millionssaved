
import React from 'react';
import ReactDOM from 'react-dom';
import CaseStudiesList from './CaseStudiesList';
import checkForNestedNeeds from '../lib/nestedNeedsChecker';
import { Link } from 'react-router';

import GetBook from './caseStudies/utilities/GetBook';

var CircleElement = (props) => (<div className='circle-element'>{props.sign}</div>);

class Home extends React.Component {
	constructor() {
		super();

		this.navigateToSection = this.navigateToSection.bind(this);
	}
	navigateToSection(section) {
		var targetSection = ReactDOM.findDOMNode(this.refs[section]);
		var offset = Math.abs(targetSection.offsetTop - window.pageYOffset);
		Velocity(targetSection, 'scroll', {duration: (1000/660*offset), easing: 'swing'});
	}
	scrollNavigate() {
		arguments[1].preventDefault();
		this.navigateToSection(arguments[0]);
	}
	componentDidMount() {
		var header = this.refs.pageHeader;
	}
	render() {
		return (
			<div>
				<header className='banner banner--home'>
					<img src='/img/about/ms-cover.jpg'/>
					<div className='banner-overlay text--center'>
						<div className='container--narrow y-center clearfix'>
							<div className='col col-md-10 col-md-offset-1 col-lg-12 col-lg-offset-0 px1' ref='pageHeader'>
								<h1 className='display--4 element--0'>Millions Saved</h1>
								<p className='lead visible--sm element--1'>Since 2004, the Center for Global Development has been collecting success stories in global health—remarkable cases in which large-scale efforts to improve health in developing countries have succeeded—and releasing them in the book <span className='text--yellow'>Millions Saved</span></p>
								<a href='#main' onClick={this.scrollNavigate.bind(this, 'main')} className='btn bg--yellow px2 py1 text--default element--2'>See what works in global health</a>
							</div>
						</div>
					</div>
					<div className='img-source'>
						<a href='https://www.flickr.com/photos/mkh_ewu/' target='_blank'>Photo by Kamrul Hasan</a>
					</div>
				</header>

				<a name='main' ref='main'></a>
				<CaseStudiesList navigateToSection={this.navigateToSection} />

				<div className='banner banner--cover'>
					<img src='/img/about/ms-home-about.jpg'/>
					<div className='banner-overlay text--center'>
						<div className='container--narrow y-center clearfix'>
							<div className='col col-md-10 col-md-offset-1 col-lg-12 col-lg-offset-0 px1'>
								<h2>
									<span className='subhead text--yellow text--uppercase'>What is</span><br /><hr className='divider--sm' />
									<span className='display--2'>Millions Saved?</span>
								</h2>
								<p className='lead visible--sm'>Millions Saved profiles 18 remarkable cases in which large-scale efforts to improve health in low- and middle-income countries succeeded, and 4 examples of promising interventions that fell short of their health targets when scaled-up in real world conditions.</p>
								<Link to='/about' className='btn bg--yellow px2 py1 mt1 text--default'>Find Out More</Link>
							</div>
						</div>
					</div>
				</div>

				<div className='bg--teal'>
					<div className='container px1 px-sm-2 py3'>
						<div className='mxn1 mx-sm-n2 clearfix text--center'>
							<div className='col col-md-12 px1 px-sm-2'>
								<h2 className='display--2'>
									<span className='subhead text--yellow text--uppercase'>Millions Saved</span><br /><hr className='divider--sm' />
									<span className='display--2'>FAQ</span>
								</h2>
								<p className='subhead visible--sm element--1 text--yellow text--uppercase'>Everything you need to know</p>
							</div>
							<div className='col col-md-4 px1 px-sm-2 py1'>
								<CircleElement sign='?' />
								<h3 className='title'>What is Millions Saved? How did it get started?</h3>
							</div>
							<div className='col col-md-4 px1 px-sm-2 py1'>
								<CircleElement sign='?' />
								<h3 className='title'>What is the difference between the various editions of Millions Saved?</h3>
							</div>
							<div className='col col-md-4 px1 px-sm-2 py1'>
								<CircleElement sign='?' />
								<h3 className='title'>How did you choose the programs to include in the book?</h3>
							</div>
							<div className='col col-md-12 px1 px-sm-2 py2'>
								<Link to='/frequently-asked-questions' className='btn bg--yellow px2 py1 text--default'>Get the Answers</Link>
							</div>
						</div>
					</div>
				</div>

				<GetBook />

			</div>
		);
	}
}

Home.needs = checkForNestedNeeds(CaseStudiesList);

export default Home;
