
import React from 'react';
import { Link } from 'react-router';

class Cases extends React.Component {
	render() {
		return (
			<div>

				<div className='bg--grey'>
					<div className='container--narrow text--center px1 px-sm-2 pt1 pb3'>
						
						<div className='mxn1 mx-sm-n2 clearfix text--center'>
							<div className='col col-md-12 px1 px-sm-2'>
								<h2>
									{ /* <span className='subhead text--yellow text--uppercase'>Millions Saved</span><br /> */ } <hr className='divider--sm' />
									<span className='display--2'>The Cases</span>
								</h2>
							</div>
						</div>

						<p className='lead'>The book profiles 22 remarkable cases in which large-scale efforts to improve health in low- and middle-income countries succeeded, and 4 examples of promising interventions that fell short of their health targets when scaled-up in real world conditions.</p>
					</div>
				</div>

				<div className='bg--teal py2'>
					<div className='container px1 px-sm-2'>
						<div className='mxn1 mx-sm-n2 clearfix'>
							<div className='col col-md-9 px1 px-sm-2 title'>
								<p className='m0 py1'>Get the book and read about 22 cases describing what works in global health.</p>
							</div>
							<div className='col col-md-3 px1 px-sm-2 title'>
								<a href='#' className='btn bg--yellow px2 py1 text--default'>Get the Book</a>
							</div>
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 pt4'>
					<div className='mxn1 mx-sm-n2 clearfix'>
						<div className='col col-md-6 px1 px-sm-2 body-text'>
							{ /* <h3 className='title mt0'>A Health Revolution</h3> */ }
							<p>Each case demonstrates how much effort —and sometimes luck—is required to fight illness and sustain good health. Sometimes technology can be the game changer; far more often, however, success emerges from wise strategic choices, quality analysis, and sound leadership. Together, the cases offer lessons about what it takes – and what it will take in the future – to bring good health to all.</p>
						</div>
						<div className='col col-md-6 px1 px-sm-2 text--center'>
							<hr className='divider--sm' />
							<p className='display--4 mb1'><strong>22</strong></p>
							<p className='subhead visible--sm element--1 text--yellow text--uppercase'>Remarkable Cases</p>
							<p>in which large-scale efforts to improve health in low- and middle- income countries succeeded.</p>
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 mb3'>
					<div className='mxn1 mx-sm-n2 clearfix'>
						<div className='col col-md-6 px1 px-sm-2 text--center'>
							<hr className='divider--sm' />
							<p className='display--4 mb1'><strong>10</strong></p>
							<p className='subhead visible--sm element--1 text--yellow text--uppercase'>Special Cases on the Website</p>
							<p>The website features 10 programs that moved the needle on one or several health outcomes. Similar to the book, the ten cases highlighted on the website defines the health problem, lays out the facts of the policy or program, describes the health impact, considers the cost of achieving that impact, summarizes the keys to lasting success, and analyzes the case’s implications for global health more broadly. The website utilizes shortened narratives, videos, images, and data visualizations to tell each success story.</p>
						</div>
						<div className='col col-md-6 px1 px-sm-2 body-text'>
							<h3 className='title mt0'>Four Main Categories</h3>
							<p>The cases are grouped into four main categories, reflecting the diversity of strategies to improve population health in low-and middle-income countries: (1) rolling out medicines and technologies; (2) expanding access to health services; (3) targeting cash transfers to improve health; and (4) promoting population-wide behavior change to decrease risk.</p>
							<h3 className='title mt0'>Across the Globe</h3>
							<p>The cases show that health success is possible anywhere given the right circumstances. They come from most world regions: seven from sub-Saharan Africa, six from Latin America and the Caribbean, five from East and South-East Asia, and four from South Asia. They also come from an economically diverse range of countries, including the poorest countries and regions in the world.</p>
						</div>
					</div>
				</div>

				<div className='bg--grey pb4'>

					<div className='container--narrow px1 px-sm-2 pb2'>
						<div className='mxn1 mx-sm-n2 clearfix text--center'>
							<div className='col col-md-12 px1 px-sm-2'>
								<h2>
									<span className='subhead text--yellow text--uppercase'>Millions Saved</span><br /><hr className='divider--sm' />
									<span className='display--2'>Selection Criteria</span>
								</h2>

								<div className='dcp3-logo px3 py1'>
									<a href='http://dcp-3.org/' target='_blank'>
										<img src='/img/about/dcp3-logo.jpg' />
									</a>
								</div>

							</div>
						</div>
					</div>

					<div className='container--narrow text--center px1 px-sm-2 pb3'>
						<p className='lead'>Cases were selected following a comprehensive literature review, expert consultations, public calls for proposals, and consultations with the Disease Control Priorities in Developing Country (DCP3) editors, an expert advisory group, and other reviewers. Criteria included:</p>
					</div>

					<div className='container px1 px-sm-2'>
						<div className='mxn1 mx-sm-n2 clearfix text--center'>
							<div className='col col-sm-6 col-lg-3 px1 px-sm-2 py2 criteria__item'>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Importance</span>
								</h4>
								<p>The intervention addressed a problem of public health significance. Mortality, morbidity, or another standardized measure such as disability-adjusted life years (DALYs) were used to indicate importance.</p>
							</div>

							<div className='col col-sm-6 col-lg-3 px1 px-sm-2 py2 criteria__item'>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Impact</span>
								</h4>
								<p>Interventions or programs demonstrated a significant and attributable impact on one or more population health outcomes based on currently available evidence.</p>
							</div>

							<div className='col col-sm-6 col-lg-3 px1 px-sm-2 py2 criteria__item'>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Equity</span>
								</h4>
								<p>Priority was given to programs that are pro-poor and include specific measures to reduce the barrier that prevent those disadvantaged by gender inequality, geography, ethnicity, from accessing health benefits.</p>
							</div>

							<div className='col col-sm-6 col-lg-3 px1 px-sm-2 py2 criteria__item'>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Scale</span>
								</h4>
								<p>Interventions were implemented on a significant scale – mostly national; regional was also considered. Programs were characterized as national if they had strong national-level commitment even if targeting a limited area or sub-group.</p>
							</div>

							<div className='col col-sm-6 col-lg-3 px1 px-sm-2 py2 criteria__item'>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Relevance</span>
								</h4>
								<p>Case information was of interest and programmatically relevant in other settings.</p>
							</div>

							<div className='col col-sm-6 col-lg-3 px1 px-sm-2 py2 criteria__item'>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Economic Evaluation</span>
								</h4>
								<p>Preference was given to programs that could show cost-effectiveness in implementation, as determined by a country-based threshold.</p>
							</div>

							<div className='col col-sm-6 col-lg-3 px1 px-sm-2 py2 criteria__item'>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Financial Protection</span>
								</h4>
								<p>Interventions that aimed to reduce the financial hardship and impoverishment associated with health problems were given priority.</p>
							</div>

							<div className='col col-sm-6 col-lg-3 px1 px-sm-2 py2 criteria__item'>
								<h4>
									<hr className='divider--sm' />
									<span className='title'>Duration</span>
								</h4>
								<p>Interventions functioned at scale for at least five years.</p>
							</div>

						</div>
					</div>

				</div>

			</div>
		);
	}
}

export default Cases;
