
import React from 'react';
import { Link } from 'react-router';

class Background extends React.Component {
	render() {
		return (
			<div>

				<div className='bg--grey'>
					<div className='container--narrow text--center px1 px-sm-2 pt1 pb3'>

						<div className='mxn1 mx-sm-n2 clearfix text--center'>
							<div className='col col-md-12 px1 px-sm-2'>
								<h2>
									{ /* <span className='subhead text--yellow text--uppercase'>Millions Saved</span><br /> */ } <hr className='divider--sm' />
									<span className='display--2'>Background</span>
								</h2>
							</div>
						</div>

						<p className='lead'>Millions Saved is a project led by the Center for Global Development. The newest edition of the book, titled, Millions Saved: New Cases of Proven Success in Global Health, was published in 2016 and authored by Amanda Glassman and Miriam Temin with the Millions Saved Team and Advisory Group. This edition follows on <a href='http://www.cgdev.org/initiative/millions-saved' target='_blank'>two previous editions</a>, published in 2004 and 2007.</p>
					</div>
				</div>

				<div className='bg--teal py2'>
					<div className='container px1 px-sm-2'>
						<div className='mxn1 mx-sm-n2 clearfix'>
							<div className='col col-md-9 px1 px-sm-2 title'>
								<p className='m0 py1'>Get the book and read about 22 cases describing what works in global health.</p>
							</div>
							<div className='col col-md-3 px1 px-sm-2 title'>
								<a href='#' className='btn bg--yellow px2 py1 text--default'>Get the Book</a>
							</div>
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 pt4'>
					<div className='mxn1 mx-sm-n2 clearfix body-text'>
						<div className='col col-md-6 px1 px-sm-2'>
							<h3 className='title mt0'>A Global Health Revolution</h3>
							<p>Since the turn of the 21st century, people in low- and middle-income countries have experienced a health revolution, one that has created new opportunities and brought new challenges. It is a revolution that keeps mothers and babies alive, helps children grow, and enables adults to thrive through and beyond their productive years.</p>
							<h3 className='title'>The Urgent Task Ahead</h3>
							<p>Yet that same health revolution has left many people behind, particularly those who are disadvantaged by the circumstances of their births. The urgent task ahead is to sustain and deepen health improvements in all regions of the world while finding creative ways to support better health among people who still suffer from exclusion and deprivation.</p>
						</div>
						<div className='col col-md-6 px1 px-sm-2'>
							<img src='/img/about/about-book.jpg' />
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 pb4'>
					<div className='mxn1 mx-sm-n2 clearfix body-text'>
						<div className='col col-md-6 px1 px-sm-2'>
							<blockquote className='mt0 py2 display--1'>
								<span className='text--yellow'>Millions Saved: New Cases of Proven Success in Global Health</span> chronicles the global health revolution from the ground up, showcasing 22 of the local, national, and regional health programs that have been part of this global change.
							</blockquote>
						</div>
						<div className='col col-md-6 px1 px-sm-2'>
							<h3 className='title mt0'>Financing the Revolution</h3>
							<p>Economic conditions have sparked the health revolution. Low- and middle-income countries’ economies have grown faster than those of their wealthier counterparts, and even the worst-off families have seen their living standards rise as national incomes have grown. Aid has also played a role. Foreign aid for health from public and private sources expanded fivefold between 1990 and 2013. And the arrival of new global health funders has fostered innovation and enabled delivery of health technologies even in the most impoverished and conflict-prone places in the world.</p>
						</div>
					</div>
				</div>

			</div>
		);
	}
}

export default Background;
