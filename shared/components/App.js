
import React from 'react';
import { Link } from 'react-router';
import Navigation from './Navigation';

import checkForNestedNeeds from '../lib/nestedNeedsChecker';

class App extends React.Component {
	// componentWillReceiveProps() {}
	// componentDidUpdate() {}
	// componentDidMount() {}
	constructor(props) {
		super(props);
		
		this.state = {
			navigationActive: false
		};

		this.toggleNavigation = this.toggleNavigation.bind(this);
		this.forceCloseNavigation = this.forceCloseNavigation.bind(this);
	}
	componentDidMount() {
		window.addEventListener('resize', () => {
			this.forceCloseNavigation();
		});
	}
	forceCloseNavigation() {
		this.setState({
			navigationActive: false
		});
	}
	toggleNavigation() {
		this.setState({
			navigationActive: !this.state.navigationActive
		});
	}
	render() {

		var wrapperStyles = {
			height: this.state.navigationActive ? window.innerHeight : 'auto',
			overflow: this.state.navigationActive ? 'hidden' : 'visible'
		};

		return (
			<div style={wrapperStyles}>
				<header className='site-header'>
					<div className={this.state.navigationActive ? 'container px-xs-1 bg--teal' : 'container px-xs-1'}>
						<div className='nav-wrapper clearfix'>
							<div className='logo'>
								<a href='http://www.cgdev.org/' target='_blank'><img className='cgd-logo' src='/img/cgd-logo.png' height='61' alt='Center for Global Development'/></a>
								<h1 className='ms-logo'><Link to='/'>Millions<br /><span className='text--yellow'>Saved</span></Link></h1>
							</div>
							<Navigation toggleNavigation={this.toggleNavigation} navigationActive={this.state.navigationActive} forceCloseNavigation={this.forceCloseNavigation} />
							<div className='get-book visible--sm'>
								<a href='#' className='btn bg--yellow px2 py1' style={{marginTop: '2.2rem',float: 'right'}}>Get the Book</a>
							</div>
						</div>
					</div>
				</header>

				<section className='wrapper-section'>
					{this.props.children}
				</section>

				<footer className='bg--grey'>
					<div className='container px1 pt4 pb3'>
						<div className='mxn1 clearfix'>

							<div className='col col-md-3 px1'>
								<p>For more information about the Center for Global Development, visit <a href='http://www.cgdev.org.'>CGDEV.org</a>.</p>
							</div>

							<div className='col col-md-3 px1'>
								<p>Sign up for CGD’s Global Health Policy newsletter</p>
								<a href='http://www.cgdev.org/page/global-health-newsletter' className='btn bg--yellow py05 px1'>Sign up</a>
							</div>

							<div className='col col-md-3 px1'>
								<p>Quick links</p>
								<ul className='m0 p0 li--unstyled'>
									<li><Link to='/about'>About</Link></li>
									<li><Link to='/findings'>Findings</Link></li>
									<li><Link to='/frequently-asked-questions'>FAQ</Link></li>
								</ul>
							</div>

							<div className='col col-md-3 px1'>
								<p>Engage with us</p>
								<ul className='m0 p0 li--unstyled'>
									<li><a href='http://twitter.com' target='_blank'>Twitter</a></li>
									<li><a href='http://facebook.com' target='_blank'>Facebook</a></li>
								</ul>
							</div>

							<div className='col col-md-12 px1 pt3 text--center'>
								<p className='text--muted m0'>&copy; 2015 — Center for Global Development</p>
							</div>

						</div>
					</div>
				</footer>
			</div>
		);
	}
}

App.needs = checkForNestedNeeds(Navigation);

export default App;
