
import React from 'react';
import { connect } from 'react-redux';
import { fetchPostsIfNeeded } from '../actions/postActions';

class PostList extends React.Component {
	render() {
		return (
			<div className='container p-x-2 p-y-4'>
				<h2>Here are some posts</h2>
				{ this.props.posts.map((post) => {
					return (<article key={post._id}>{post.title}</article>);
				})}
			</div>
		);
	}
}

PostList.needs = [ fetchPostsIfNeeded ];

function mapStateToProps(state) {
	return {
		posts: state.postReducer.posts
	};
}

export default connect(mapStateToProps)(PostList);
