
import React from 'react';
import ReactDOM from 'react-dom';
import Row from './utilities/Row';
import Col from './utilities/Column';
import Paragraph from './utilities/Paragraph';
import Break from './utilities/Break';
import BreakSlider from './utilities/SouthAfricaBreakSlider';

import MiniMap from './utilities/MiniMap';
import MiniVisualization from './utilities/MiniVisualization';
import CaseStudyNavigation from './utilities/CaseStudyNavigation';

import CaseStudyCover from './utilities/CaseStudyCover';
import CaseStudySummary from './utilities/CaseStudySummary';

import SouthAfricaCSGVisualization from './mainVisualizations/SouthAfricaCSG.visualization';

import classNames from 'classnames';
import bootstrapSourcing from './utilities/sourcing';

//
// South Africa Child Support Grant Case Study
//
class Case extends React.Component {
	navigateToSection(section) {
		var targetSection = ReactDOM.findDOMNode(this.refs[section]);
		var offset = Math.abs(targetSection.offsetTop - window.pageYOffset);
		Velocity(targetSection, 'scroll', {duration: (1000/2600*offset), easing: 'swing', offset: -72});
	}

	componentDidMount() {
		if(!this.props.caseStudy.visualizationsLoaded) {
			this.props.loadMiniVisualizations(this.props.caseStudy);	
		}

		bootstrapSourcing(document.getElementsByTagName('sup'));
	}

	render() {

		var caseStudy = this.props.caseStudy;

		var background        = caseStudy.background,
				programRollout    = caseStudy.programRollout,
				impact            = caseStudy.impact,
				cost              = caseStudy.cost,
				reasonsForSuccess = caseStudy.reasonsForSuccess,
				implications      = caseStudy.implications;

		var sectionStyle = 'container--narrow px1 px-sm-2';
		var titleStyle   = 'title mt0';

		var sliderItems = [
			// {
			// 	text: 'Per Capita Income (1993, in 2000 RAND)',
			// 	white: 46486,
			// 	black: 5073
			// },
			// {
			// 	text: 'Per Capita Income (1993, in 2000 USD)',
			// 	white:	7555.63,
			// 	black: 824.50
			// },
			{
				verbose: 'In 1993, Black South Africans were earning approximately 10 times less than White South Africans.',
				text: 'Per Capita Income (1993, in 2015 USD)',
				white:	10298.91,
				black: 1123.86,
				note: '<p>* Numbers listed in 2015 US$ and adjusted for inflation using <a href="http://data.bls.gov/cgi-bin/cpicalc.pl?cost1=824.50&year1=2000&year2=2015" target="_blank">this site</a>.</p>'
			},
			{
				verbose: 'On average there were 3x more Black South Africans living per room than White South Africans in 1993.',
				text: 'People per room',
				white:	0.52,
				black: 1.65
			},
			{
				verbose: '99.9% of White South Africans had access to indoor tap water, but only 19.1% of Black South Africans had access to indoor tap water.',
				text: 'Percent with indoor tap water',
				white: '99.90%',
				black: '19.10%'
			},
			{
				verbose: 'While 100% of White South Africans had access to flush toilets in 1993, only 42.2% of Black South Africans had access to flush toilets.',
				text: 'Percent with flush toilet',
				white: '100%',
				black: '42.20%'
			},
			{
				verbose: 'Roughly 40% of Black South Africans had access to electricity in 1993. 100% of White South Africans had access to electricity that same year.',
				text: 'Percent with electricity',
				white: '100%',
				black: '40.80%'
			},
			{
				verbose: 'Just about 10% of Black South Africans had health insurance in 1993, while 76% of White South Africans had health insurance.',
				text: 'Percent with health insurance',
				white: '76%',
				black: '10%'
			},
			{
				verbose: 'Only 61% of Black South African children had birth certificates in 1993, as opposed to 99% of White South African children.',
				text: 'Percent of children with birth certificate',
				white: '99%',
				black: '61%'
			},
			{
				verbose: 'In 1993, only 13% of Black South Africans graduated from high school. 65% of White South Africans graduated from high school the same year.',
				text: 'Percent  high school graduates',
				white: '65%',
				black: '13%'
			},
			{
				verbose: '32% of White South Africans were unemployed, while only 8% of Black South Africans were unemployed in 1993.',
				text: 'Percent unemployed',
				white: '32%',
				black: '8%'
			}
		];

		return (
			<div>

				<CaseStudyCover caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudySummary caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudyNavigation navigateToSection={this.navigateToSection.bind(this)}/>

				<div id='case-study-background'>
					<div className={classNames(sectionStyle, 'mt4')} ref='background'>
						<a name='background'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Background</h2>
								<p className='lead'>{ this.props.caseStudy.lead }</p>
							</Col>
							<Col>
								<MiniMap region={this.props.caseStudy.region} template={this.props.caseStudy.template} />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.background.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.background.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.background.visualizationOne } />
							</Col>
						</Row>
					</div>
					
					<BreakSlider slides={sliderItems}/>
				</div>

				<div id='case-study-program-rollout'>
					<div className={sectionStyle} ref='programRollout'>
						<a name='program-rollout'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Program Rollout</h2>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFour } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFive } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSix } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationThree } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationFour } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSeven } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphEight } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakTwo} />
				</div>

				<div id='case-study-impact'>
					<div className={sectionStyle} ref='impact'>
						<a name='impact'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Impact</h2>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.impact.visualizationOne } />
							</Col>
						</Row>
					</div>

					<SouthAfricaCSGVisualization source={caseStudy.mainVisualizationSource} />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.impact.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphFour } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakThree} />
				</div>

				<div id='case-study-cost'>
					<div className={sectionStyle} ref='cost'>
						<a name='cost'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Cost</h2>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.cost.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.cost.visualizationOne } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFour} />
				</div>

				<div id='case-study-reasons-for-success'>
					<div className={sectionStyle} ref='reasonsForSuccess'>
						<a name='reasons-for-success'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Reasons for Success</h2>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphOne } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationOne } />
							</Col>
						</Row>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationTwo } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationThree } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphFour } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphFive } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphSix } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationFour } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationFive } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphSeven } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphEight } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFive} />
				</div>

				<div id='case-study-implications'>
					<div className={sectionStyle} ref='implications'>
						<a name='implications'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Implications</h2>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.implications.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container mb3 clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-5/south-africa-csg-2.jpg' />
							</div>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 col-md-offset-6 px1 px-sm-2 bg--white case-study-img-text'>
									<Paragraph markup={ this.props.caseStudy.implications.paragraphThree } />
								</div>
							</Row>
						</div>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphFour } />
								<Paragraph markup={ this.props.caseStudy.implications.paragraphFive } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphSix } />
								<Paragraph markup={ this.props.caseStudy.implications.paragraphSeven } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationThree } />
							</Col>
						</Row>
					</div>

					<div className='bg--teal'>
						<div className="conclusion-mini-map">
							<MiniMap region={caseStudy.region} template={caseStudy.template} conclusion={true} />
						</div>
					</div>

					<Break break={caseStudy.conclusion} conclusion={true} />
				</div>

			</div>
		);
	}
}

export default Case;
