
import React from 'react';
import ReactDOM from 'react-dom';
import Row from './utilities/Row';
import Col from './utilities/Column';
import Paragraph from './utilities/Paragraph';
import Break from './utilities/Break';
import Video from './utilities/Video';

import MiniMap from './utilities/MiniMap';
import MiniVisualization from './utilities/MiniVisualization';
import CaseStudyNavigation from './utilities/CaseStudyNavigation';

import CaseStudyCover from './utilities/CaseStudyCover';
import CaseStudySummary from './utilities/CaseStudySummary';

import VietnamHelmetsVisualization from './mainVisualizations/VietnamHelmets.visualization';

import classNames from 'classnames';
import bootstrapSourcing from './utilities/sourcing';

//
// Vietnam Helmet Law Case Study
//
class Case extends React.Component {
	navigateToSection(section) {
		var targetSection = ReactDOM.findDOMNode(this.refs[section]);
		var offset = Math.abs(targetSection.offsetTop - window.pageYOffset);
		Velocity(targetSection, 'scroll', {duration: (1000/2600*offset), easing: 'swing', offset: -72});
	}

	componentDidMount() {
		if(!this.props.caseStudy.visualizationsLoaded) {
			this.props.loadMiniVisualizations(this.props.caseStudy);	
		}

		bootstrapSourcing(document.getElementsByTagName('sup'));
	}

	render() {

		var caseStudy = this.props.caseStudy;

		var background        = caseStudy.background,
				programRollout    = caseStudy.programRollout,
				impact            = caseStudy.impact,
				cost              = caseStudy.cost,
				reasonsForSuccess = caseStudy.reasonsForSuccess,
				implications      = caseStudy.implications;

		var sectionStyle = 'container--narrow px1 px-sm-2';
		var titleStyle   = 'title mt0';

		return (
			<div>

				<CaseStudyCover caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudySummary caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudyNavigation navigateToSection={this.navigateToSection.bind(this)}/>

				<div id='case-study-background'>
					<div className={classNames(sectionStyle, 'mt4')} ref='background'>
						<a name='background'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Background</h2>
								<p className='lead'>{ this.props.caseStudy.lead }</p>
							</Col>
							<Col>
								<MiniMap region={this.props.caseStudy.region} template={this.props.caseStudy.template} />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.background.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.background.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.background.visualizationOne } />
							</Col>
						</Row>
					</div>

					<Video videoSource='https://www.youtube.com/embed/acwIXeg-240' videoType='youTube' videoCover='/img/case-studies/case-7/vietnam-video-cover1.jpg' />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.background.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.background.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.background.paragraphFour } />
								<Paragraph markup={ this.props.caseStudy.background.paragraphFive } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakOne} />
				</div>

				<div id='case-study-program-rollout'>
					<div className={sectionStyle} ref='programRollout'>
						<a name='programRollout'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Program Rollout</h2>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphThree } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFour } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFive } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSix } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationThree } />
							</Col>
						</Row>
					</div>

					<Video videoSource='https://www.youtube.com/embed/JJvMo8OM3Ig' videoType='youTube' videoCover='/img/case-studies/case-7/vietnam-video-cover2.jpg' />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationFour } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSeven } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphEight } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphNine } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakTwo} />
				</div>

				<div id='case-study-impact'>
					<div className={sectionStyle} ref='impact'>
						<a name='impact'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Impact</h2>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphOne } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.impact.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.impact.visualizationTwo } />
							</Col>
						</Row>
					</div>

					<VietnamHelmetsVisualization currentYear={0} source={caseStudy.mainVisualizationSource} />

					<div className={sectionStyle}>
						<Row>
							<div className='col col-md-6 col-md-offset-6 px1 px-sm-2'>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphFour } />
							</div>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakThree} />
				</div>

				<div id='case-study-cost'>
					<div className={sectionStyle} ref='cost'>
						<a name='cost'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Cost</h2>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.cost.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.cost.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphThree } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.cost.visualizationTwo } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFour} />
				</div>

				<div id='case-study-reasons-for-success'>
					<div className={sectionStyle} ref='reasonsForSuccess'>
						<a name='reasons-for-success'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Reasons for Success</h2>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphFour } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container mb3 clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-7/bike-vietnam.jpg' />
							</div>
						</div>
						<div className='img-source'>
							<a href="#">John Smith — flickr account name...</a>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 px1 px-sm-2 bg--white case-study-img-text'>
									<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphFive } />
								</div>
							</Row>
						</div>
					</div>

					<div className={sectionStyle}>
						<Row>
							<div className='col col-md-6 col-md-offset-6 px1 px-sm-2'>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphSix } />
							</div>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFive} />
				</div>

				<div id='case-study-implications'>
					<div className={sectionStyle} ref='implications'>
						<a name='implications'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Implications</h2>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.implications.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.implications.paragraphFour } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-7/bikes-vietnam.jpg' />
							</div>
						</div>
						<div className='img-source'>
							<a href="#">John Smith — flickr account name...</a>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 px1 px-sm-2 bg--white case-study-img-text'>
									<Paragraph markup={ this.props.caseStudy.implications.paragraphFive } />
								</div>
							</Row>
						</div>
					</div>

					<div className='bg--teal'>
						<div className="conclusion-mini-map">
							<MiniMap region={caseStudy.region} template={caseStudy.template} conclusion={true} />
						</div>
					</div>

					<Break break={caseStudy.conclusion} conclusion={true} />
				</div>

			</div>
		);
	}
}

export default Case;
