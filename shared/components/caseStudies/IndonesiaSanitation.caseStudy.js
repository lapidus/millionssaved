
import React from 'react';
import ReactDOM from 'react-dom';
import Row from './utilities/Row';
import Col from './utilities/Column';
import Paragraph from './utilities/Paragraph';
import Break from './utilities/Break';
import Video from './utilities/Video';

import CaseStudyNavigation from './utilities/CaseStudyNavigation';
import MiniVisualization from './utilities/MiniVisualization';
import MiniMap from './utilities/MiniMap';

import CaseStudyCover from './utilities/CaseStudyCover';
import CaseStudySummary from './utilities/CaseStudySummary';

import IndonesiaSanitationVisualization from './mainVisualizations/IndonesiaSanitation.visualization';

import classNames from 'classnames';
import bootstrapSourcing from './utilities/sourcing';

//
// Indonesia Sanitation Case Study
//
class Case extends React.Component {
	navigateToSection(section) {
		var targetSection = ReactDOM.findDOMNode(this.refs[section]);
		var offset = Math.abs(targetSection.offsetTop - window.pageYOffset);
		Velocity(targetSection, 'scroll', {duration: (1000/2600*offset), easing: 'swing', offset: -72});
	}

	componentDidMount() {
		if(!this.props.caseStudy.visualizationsLoaded) {
			this.props.loadMiniVisualizations(this.props.caseStudy);	
		}

		bootstrapSourcing(document.getElementsByTagName('sup'));
	}

	render() {

		var caseStudy = this.props.caseStudy;

		var background        = caseStudy.background,
				programRollout    = caseStudy.programRollout,
				impact            = caseStudy.impact,
				cost              = caseStudy.cost,
				reasonsForSuccess = caseStudy.reasonsForSuccess,
				implications      = caseStudy.implications;

		var sectionStyle = 'container--narrow px1 px-sm-2';
		var titleStyle   = 'title mt0';

		return (
			<div>

				<CaseStudyCover caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudySummary caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudyNavigation navigateToSection={this.navigateToSection.bind(this)}/>

				<div id='case-study-background'>
					<div className={classNames(sectionStyle, 'mt4')} ref='background'>
						<a name='background'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Background</h2>
								<p className='lead'>{ caseStudy.lead }</p>
							</Col>
							<Col>
								<MiniMap region={ caseStudy.region } template={ caseStudy.template } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ background.paragraphOne } />
								<Paragraph markup={ background.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ background.visualizationOne } />
							</Col>
						</Row>
					</div>

					<Video videoSource='https://www.youtube.com/embed/4vROpJLVQo8' videoType='youTube' videoCover='/img/case-studies/case-6/indonesia-video-cover.jpg' />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ background.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ background.paragraphThree } />
							</Col>
						</Row>
					</div>

					<Break break={caseStudy.breakOne} />
				</div>

				<div id='case-study-program-rollout'>
					<div className={sectionStyle} ref='programRollout'>
						<a name='program-rollout'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Program Rollout</h2>
								<Paragraph markup={ programRollout.paragraphOne } />
								<Paragraph markup={ programRollout.paragraphTwo } />
								<Paragraph markup={ programRollout.paragraphThree } />
							</Col>
							<Col>
								<MiniVisualization visualization={ programRollout.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ programRollout.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ programRollout.paragraphFour } />
								<Paragraph markup={ programRollout.paragraphFive } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ programRollout.paragraphSix } />
								<Paragraph markup={ programRollout.paragraphSeven } />
							</Col>
							<Col>
								<MiniVisualization visualization={ programRollout.visualizationThree } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ programRollout.visualizationFour } />
							</Col>
							<Col>
								<Paragraph markup={ programRollout.paragraphEight } />
								<Paragraph markup={ programRollout.paragraphNine } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ programRollout.paragraphTen } />
								<Paragraph markup={ programRollout.paragraphEleven } />
							</Col>
							<Col>
								<MiniVisualization visualization={ programRollout.visualizationFive } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ programRollout.visualizationSix } />
							</Col>
							<Col>
								<Paragraph markup={ programRollout.paragraphTwelve } />
								<Paragraph markup={ programRollout.paragraphThirteen } />
							</Col>
						</Row>
					</div>

					<Break break={caseStudy.breakTwo} />
				</div>

				<div id='case-study-impact'>
					<div className={sectionStyle} ref='impact'>
						<a name='impact'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Impact</h2>
								<Paragraph markup={ impact.paragraphOne } />
								<Paragraph markup={ impact.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ impact.visualizationOne } />
							</Col>
						</Row>
					</div>

					<IndonesiaSanitationVisualization source={caseStudy.mainVisualizationSource} />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ impact.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ impact.paragraphThree } />
							</Col>
						</Row>
					</div>

					<Break break={caseStudy.breakThree} />
				</div>

				<div id='case-study-cost'>
					<div className={sectionStyle} ref='cost'>
						<a name='cost'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Cost</h2>
								<Paragraph markup={ cost.paragraphOne } />
								<Paragraph markup={ cost.paragraphTwo } />
								<Paragraph markup={ cost.paragraphThree } />
							</Col>
							<Col>
								<MiniVisualization visualization={ cost.visualizationOne } />
							</Col>
						</Row>
					</div>

					<Break break={caseStudy.breakFour} />
				</div>

				<div id='case-study-reasons-for-success'>
					<div className={sectionStyle} ref='reasonsForSuccess'>
						<a name='reasons-for-success'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Reasons for Success</h2>
								<Paragraph markup={ reasonsForSuccess.paragraphOne } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container mb3 clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-6/indonesia-sanitation-2.jpg' />
							</div>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 col-md-offset-6 px1 px-sm-2 bg--white case-study-img-text'>
									<Paragraph markup={ reasonsForSuccess.paragraphTwo } />
								</div>
							</Row>
						</div>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ reasonsForSuccess.visualizationOne } />
							</Col>
							<Col>
								<Paragraph markup={ reasonsForSuccess.paragraphThree } />
								<Paragraph markup={ reasonsForSuccess.paragraphFour } />
							</Col>
						</Row>
					</div>

					<Break break={caseStudy.breakFive} />
				</div>

				<div id='case-study-implications'>
					<div className={sectionStyle} ref='implications'>
						<a name='implications'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Implications</h2>
								<Paragraph markup={ implications.paragraphOne } />
							</Col>
							<Col>
								<MiniVisualization visualization={ implications.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ implications.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ implications.visualizationTwo } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ implications.visualizationThree } />
							</Col>
							<Col>
								<Paragraph markup={ implications.paragraphThree } />
							</Col>
						</Row>
					</div>

					<div className='bg--teal'>
						<div className="conclusion-mini-map">
							<MiniMap region={caseStudy.region} template={caseStudy.template} conclusion={true} />
						</div>
					</div>

					<Break break={caseStudy.conclusion} conclusion={true} />
				</div>

			</div>
		);
	}
}

export default Case;
