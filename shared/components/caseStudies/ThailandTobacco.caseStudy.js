
import React from 'react';
import ReactDOM from 'react-dom';
import Row from './utilities/Row';
import Col from './utilities/Column';
import Paragraph from './utilities/Paragraph';
import Break from './utilities/Break';
import Video from './utilities/Video';

import MiniMap from './utilities/MiniMap';
import MiniVisualization from './utilities/MiniVisualization';
import CaseStudyNavigation from './utilities/CaseStudyNavigation';

import CaseStudyCover from './utilities/CaseStudyCover';
import CaseStudySummary from './utilities/CaseStudySummary';

import ThailandTobaccoVisualization from './mainVisualizations/ThailandTobacco.visualization';

import classNames from 'classnames';
import bootstrapSourcing from './utilities/sourcing';

//
// Thailand Tobacco Case Study
//
class Case extends React.Component {
	navigateToSection(section) {
		var targetSection = ReactDOM.findDOMNode(this.refs[section]);
		var offset = Math.abs(targetSection.offsetTop - window.pageYOffset);
		Velocity(targetSection, 'scroll', {duration: (1000/2600*offset), easing: 'swing', offset: -72});
	}

	componentDidMount() {
		if(!this.props.caseStudy.visualizationsLoaded) {
			this.props.loadMiniVisualizations(this.props.caseStudy);	
		}

		bootstrapSourcing(document.getElementsByTagName('sup'));
	}

	render() {

		var caseStudy = this.props.caseStudy;

		var background        = caseStudy.background,
				programRollout    = caseStudy.programRollout,
				impact            = caseStudy.impact,
				cost              = caseStudy.cost,
				reasonsForSuccess = caseStudy.reasonsForSuccess,
				implications      = caseStudy.implications;

		var sectionStyle = 'container--narrow px1 px-sm-2';
		var titleStyle   = 'title mt0';

		return (
			<div>

				<CaseStudyCover caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudySummary caseStudy={caseStudy} caseId={this.props.caseId} />

				<CaseStudyNavigation navigateToSection={this.navigateToSection.bind(this)}/>

				<div id='case-study-background'>
					<div className={classNames(sectionStyle, 'mt4')} ref='background'>
						<a name='background'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Background</h2>
								<p className='lead'>{ this.props.caseStudy.lead }</p>
							</Col>
							<Col>
								<MiniMap region={this.props.caseStudy.region} template={this.props.caseStudy.template} />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.background.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.background.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.background.visualizationOne } />
							</Col>
						</Row>
					</div>

					<aside className='case-study-img-container mb4 clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-8/data-visualization-preview.jpg' />
							</div>
						</div>
						<div className='container--narrow px1 px-sm-2' style={{position:'relative'}}>
							<div className='mxn1 mx-sm-n2 clearfix body-text'>
								<div className='col col-md-6 px1 px-sm-2 bg--white case-study-img-text'>
									<h1 className='typo--title'>Tobacco Burden<br />for Each Country</h1>
									<p>Use this tool, developed by the Institute for Health Metrics and Evaluation (an independent global health research center) to explore the Tobacco Burden worldwide, as well as for each country individually.</p>
									<a href='http://vizhub.healthdata.org/tobacco/' target='_blank' className='btn btn--outline py1 px1 mb2' style={{fontSize:'1.6rem'}}>Visit Visualization</a>
								</div>
							</div>
						</div>
					</aside>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.background.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.background.paragraphThree } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakOne} />
				</div>

				<div id='case-study-program-rollout'>
					<div className={sectionStyle} ref='programRollout'>
						<a name='program-rollout'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Program Rollout</h2>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphOne } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container mb3 clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-8/protest.jpg' />
							</div>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 col-md-offset-6 px1 px-sm-2 bg--white case-study-img-text'>
									<Paragraph markup={ this.props.caseStudy.programRollout.paragraphTwo } />
								</div>
							</Row>
						</div>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFour } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationTwo } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationThree } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphFive } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationFour } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSix } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationFive } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphSeven } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationSix } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphEight } />
							</Col>
						</Row>
					</div>

					<Video videoSource='https://player.vimeo.com/video/126220314?portrait=0&color=B89434&byline=0&badge=0' videoType='vimeo' videoCover='/img/case-studies/case-8/thailand-tobacco-video-cover.jpg' />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationSeven } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphNine } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationEight } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphTen } />
							</Col>
						</Row>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.programRollout.visualizationNine } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.programRollout.paragraphEleven } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakTwo} />
				</div>

				<div id='case-study-impact'>
					<div className={sectionStyle} ref='impact'>
						<a name='impact'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Impact</h2>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.impact.visualizationOne } />
							</Col>
						</Row>
					</div>

					<ThailandTobaccoVisualization source={caseStudy.mainVisualizationSource} />

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.impact.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.impact.paragraphThree } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphFour } />
								<Paragraph markup={ this.props.caseStudy.impact.paragraphFive } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakThree} />
				</div>

				<div id='case-study-cost'>
					<div className={sectionStyle} ref='cost'>
						<a name='cost'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Cost</h2>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.cost.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.cost.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.cost.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphThree } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFour} />
				</div>

				<div id='case-study-reasons-for-success'>
					<div className={sectionStyle} ref='reasonsForSuccess'>
						<a name='reasons-for-success'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Reasons for Success</h2>
								<Paragraph markup={ this.props.caseStudy.cost.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.cost.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className='case-study-img-container mb3 clearfix'>
						<div className='case-study-img'>
							<div className='col col-md-12' style={{overflow:'hidden'}}>
								<img src='/img/case-studies/case-8/ads.jpg' />
							</div>
						</div>
						<div className={sectionStyle} style={{position:'relative'}}>
							<Row>
								<div className='col col-md-6 col-md-offset-6 px1 px-sm-2 bg--white case-study-img-text'>
									<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphThree } />
								</div>
							</Row>
						</div>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphFour } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationTwo } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.reasonsForSuccess.visualizationThree } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphFive } />
								<Paragraph markup={ this.props.caseStudy.reasonsForSuccess.paragraphSix } />
							</Col>
						</Row>
					</div>

					<Break break={this.props.caseStudy.breakFive} />
				</div>

				<div id='case-study-implications'>
					<div className={sectionStyle} ref='implications'>
						<a name='implications'></a>
						<Row>
							<Col>
								<h2 className={titleStyle}>Implications</h2>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphOne } />
								<Paragraph markup={ this.props.caseStudy.implications.paragraphTwo } />
							</Col>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationOne } />
							</Col>
						</Row>
					</div>

					<div className={sectionStyle}>
						<Row>
							<Col>
								<MiniVisualization visualization={ this.props.caseStudy.implications.visualizationTwo } />
							</Col>
							<Col>
								<Paragraph markup={ this.props.caseStudy.implications.paragraphThree } />
							</Col>
						</Row>
					</div>

					<div className='bg--teal'>
						<div className="conclusion-mini-map">
							<MiniMap region={caseStudy.region} template={caseStudy.template} conclusion={true} />
						</div>
					</div>

					<Break break={caseStudy.conclusion} conclusion={true} />
				</div>

			</div>
		);
	}
}

export default Case;
