
import React from 'react';

class ShareButton extends React.Component {
	constructor() {
		super();

		this.renderFacebook = this.renderFacebook.bind(this);
		this.renderTwitter = this.renderTwitter.bind(this);
		this.renderMail = this.renderMail.bind(this);
		this.rootUrl = this.rootUrl.bind(this);
	}

	rootUrl() {
		return 'http://millionssaved.herokuapp.com';
	}

	renderTwitter() {

		// `http://twitter.com/intent/tweet?url=${referer}&text=${shareText}` ?

		var referer = `${this.rootUrl()}${this.props.referer}`;
		// var shareText = encodeURIComponent(`${this.props.shareText} - ${referer}`);
		// var url = `https://twitter.com/intent/tweet?text=${shareText}&original_referer=${referer}`;
		var shareText = encodeURIComponent(`${this.props.shareText} - `);
		var url = `http://twitter.com/intent/tweet?url=${referer}&text=${shareText}&original_referer=${referer}`;

		return <a href={url} className='btn btn--outline px1' target='_blank'>Tweet</a>;
	}

	renderFacebook() {

		// `http://facebook.com/sharer/sharer.php?u=${referer}&title=${shareText}` ?

		var referer = `${this.rootUrl()}${this.props.referer}`;
		var shareText = encodeURIComponent(`${this.props.shareText} - ${referer}`);
		var url = `http://facebook.com/sharer/sharer.php?u=${referer}&title=${shareText}`;

		return <a href={url} className='btn btn--outline px1' target='_blank'>Share</a>;
	}

	renderMail() {

		// `mailto:?subject=' + encodeURIComponent('Flowminder: ' + scope.share.title) + '&body=' + encodeURIComponent('Flowminder: ' + scope.share.title) + ' — ' + (shareUrl + scope.share.slug)` ?
		
		var subject = encodeURIComponent('Center for Global Development: Millions Saved');
		var referer = `${this.rootUrl()}${this.props.referer}`;
		var body = encodeURIComponent(`${this.props.shareText}\n\n${this.props.mailBody}\n\n${referer}`);
		var url = `mailto:?subject=${subject}&body=${body}`;

		return <a href={url} className='btn btn--outline px1'>Mail</a>;
	}

	render() {

		var shareBtn = this[`render${this.props.shareButtonType}`];

		return shareBtn();
	}
}

export default ShareButton;
