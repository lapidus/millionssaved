
import React from 'react';

import d3 from 'd3';
import topojson from 'topojson';

import colors from './colors';

import { connect } from 'react-redux';

import { getMap } from '../../../actions/mapActions';

//
// Case Study Mini Map
//
class MiniMap extends React.Component {

	componentDidMount() {

		var el = this.refs.miniMap;

		this.scene = new ScrollMagic.Scene({triggerElement: el, triggerHook: 'onEnter', offset: 200})
			.addTo(window.scrollMagicMasterController)
			.setClassToggle(el, 'mini-map--active');

		if(this.props.worldMap.arcs) {
			this.initMap();
		}
		else {
			this.props.dispatch(getMap());
		}

	}

	initMap() {
		var el = this.refs.miniMap;

		var mapContainer = d3.select(el);

		var width = 400,
				height = 600;

		var conclusionMap = this.props.conclusion || false;

		var regions = {
			'Africa': {
				countries: ['DZA','AGO','SHN','BEN','BWA','BFA','BDI','CMR','CPV','CAF','TCD','COM','COG','DJI','EGY','GNQ','ERI','ETH','ESH','GAB','GMB','GHA','GNB','GIN','CIV','KEN','LSO','LBR','LBY','MDG','MWI','MLI','MRT','MUS','MYT','MAR','MOZ','NAM','NER','NGA','STP','REU','RWA','STP','SEN','SYC','SLE','SOM','ZAF','SHN','SDN','SWZ','TZA','TGO','TUN','UGA','COD','ZMB','TZA','ZWE','SSD','COD'],
				offset: [width / 2.4, height / 2.1],
				scale: 320
			},
			'South-East Asia': {
				countries: ['CHN','IND','AUS','THA','VNM','LAO','BGD','JPN','PNG','PHL','MNG','NPL','KHM','MMR','BTN','KOR','PRK','MYS','IDN'],
				offset: [-width / 1.28, height / 1.85],
				scale: 290
			},
			'Latin America': {
				countries: ['ARG','BOL','BRA','CHL','COL','ECU','FLK','GUF','GUF','GUY','PRY','PER','SUR','URY','VEN','HTI','PAN','GTM','HND','NIC','CRI','DOM','DMA','JAM','FRA','BLZ','CUB','SLV','BHS', 'MEX'],
				offset: [width / 0.62, height / 2.9],
				scale: 300
			}
		};

		var region = regions[this.props.region];

		var countryHighlights = [
			{ value: 0, highlights: ['SEN', 'GMB', 'GNB', 'GIN', 'MRT', 'MLI', 'CIV', 'BFA', 'GHA', 'TGO', 'BEN', 'NER', 'NGA', 'TCD', 'CMR', 'CAF', 'SDN', 'SSD', 'COD', 'UGA', 'ERI', 'ETH', 'KEN'], label: 'MenAfriVac' },
			{ value: 1, highlights: ['BWA'], label: 'Botswana ARV' },
			{ value: 2, highlights: ['ARG'], label: 'Argentina Plan Nacer' },
			{ value: 3, highlights: ['THA'], label: 'Thailand UCS' },
			{ value: 4, highlights: ['KEN'], label: 'Kenya OVC' },
			{ value: 5, highlights: ['ZAF'], label: 'South Africa CSG' },
			{ value: 6, highlights: ['VNM'], label: 'Vietnam Helmets' },
			{ value: 7, highlights: ['IDN'], label: 'Indonesia Sanitation' },
			{ value: 8, highlights: ['THA'], label: 'Thailand Tobacco' },
			{ value: 9, highlights: ['HTI'], label: 'Haiti Polio' }
		];

		var targetHighlights = this.props.highlightCountries && this.props.highlightCountries.length > 0 ? this.props.highlightCountries : countryHighlights[this.props.template].highlights;

		var projection = d3.geo.mercator()
											 .scale(region.scale)
											 .rotate([-10,0,0])
											 .translate(region.offset);

		var path = d3.geo.path().projection(projection);

		var svg = mapContainer.append('svg')
								.attr('width', '100%')
								.attr('height', height)
								.attr('viewBox', '0 0 ' + width + ' ' + height)
								.append('g');

		var map = svg.insert('g')
								 .attr('class', 'world');

		map.append('rect')
			 .attr('class', 'overlay')
			 .attr('width', width)
			 .attr('height', height)
			 .style('fill', 'none');

		var topojsonShapes = topojson.feature(this.props.worldMap, this.props.worldMap.objects.countries).features;

		map.selectAll('.shape')
			 .data(topojsonShapes)
			 .enter()
			 .append('path')
			 .attr('class', function(d) {
					return 'country';
			 })
			 .attr('d', path)
			 .style('fill', function(d) {
					var visibility = region.countries.indexOf(d.id);
					var targetCountry = targetHighlights.indexOf(d.id);
					return targetCountry < 0 ? (visibility == -1 ? 'transparent' : conclusionMap ? colors.teal700 : colors.grey200) : colors.yellow500;
			 })
			 .style('stroke', conclusionMap ? 'transparent' : colors.white)
			 .style('stroke-width', 1);

	}

	componentDidUpdate(prevProps) {
		if(!prevProps.worldMap.arcs && this.props.worldMap.arcs) {
			this.initMap();
		}
	}

	componentWillUnmount() {
		if(this.scene) {
			this.scene.destroy(true);
		}
	}

	render() {
		return (
			<div className='mini-map' ref='miniMap'></div>
		);
	}
}

function mapStateToProps(state) {
	return {
		isFetching: state.mapReducer.isFetching,
		worldMap: state.mapReducer.worldMap,
		dispatch: state.caseStudiesReducer.dispatch
	};
}

export default connect(mapStateToProps)(MiniMap);
