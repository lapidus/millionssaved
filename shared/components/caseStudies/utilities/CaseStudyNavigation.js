
import React from 'react';
import Select from 'react-select';

//
// Case Study Navigation
//
class CaseStudyNavigation extends React.Component {
	constructor() {
		super();

		this.state = {
			active: -1
		};
	}
	scrollNavigate() {
		arguments[1].preventDefault();
		this.props.navigateToSection(arguments[0]);
	}

	scrollNavigateFromSelect(val) {
		this.props.navigateToSection(val);
	}

	componentDidMount() {
		var triggerElement = this.refs.caseStudyNavigationTrigger;
		var targetElement = this.refs.caseStudyNavigation;
		this.scene = new ScrollMagic.Scene({triggerElement: triggerElement, triggerHook: 'onLeave'})
				.addTo(window.scrollMagicMasterController)
				.setClassToggle(targetElement, 'case-study-navigation--affixed');

		var triggers = [
			document.getElementById('case-study-background'),
			document.getElementById('case-study-program-rollout'),
			document.getElementById('case-study-impact'),
			document.getElementById('case-study-cost'),
			document.getElementById('case-study-reasons-for-success'),
			document.getElementById('case-study-implications')
		];

		this.navScenes = [];

		this.navScenes[0] = new ScrollMagic.Scene({triggerElement: triggers[0], duration: triggers[0].clientHeight, triggerHook: 'onLeave', offset: -72})
				.addTo(window.scrollMagicMasterController)
				.on('enter', () => {
					this.setState({ active: 0 });
				})
				.on('leave', () => {
					this.setState({ active: -1 });
				});

		this.navScenes[1] = new ScrollMagic.Scene({triggerElement: triggers[1], duration: triggers[1].clientHeight, triggerHook: 'onLeave', offset: -72})
				.addTo(window.scrollMagicMasterController)
				.on('enter', () => {
					this.setState({ active: 1 });
				});

		this.navScenes[2] = new ScrollMagic.Scene({triggerElement: triggers[2], duration: triggers[2].clientHeight, triggerHook: 'onLeave', offset: -72})
				.addTo(window.scrollMagicMasterController)
				.on('enter', () => {
					this.setState({ active: 2 });
				});

		this.navScenes[3] = new ScrollMagic.Scene({triggerElement: triggers[3], duration: triggers[3].clientHeight, triggerHook: 'onLeave', offset: -72})
				.addTo(window.scrollMagicMasterController)
				.on('enter', () => {
					this.setState({ active: 3 });
				});

		this.navScenes[4] = new ScrollMagic.Scene({triggerElement: triggers[4], duration: triggers[4].clientHeight, triggerHook: 'onLeave', offset: -72})
				.addTo(window.scrollMagicMasterController)
				.on('enter', () => {
					this.setState({ active: 4 });
				});

		this.navScenes[5] = new ScrollMagic.Scene({triggerElement: triggers[5], duration: triggers[5].clientHeight, triggerHook: 'onLeave', offset: -72})
				.addTo(window.scrollMagicMasterController)
				.on('enter', () => {
					this.setState({ active: 5 });
				});

	}

	componentWillUnmount() {
		if(this.scene) {
			this.scene.destroy(true);
		}
		if(this.navScenes) {
			this.navScenes.forEach(function(scene) {
				scene.destroy(true);
			});
		}
	}

	render() {

		var sections = [
			{
				href: '#background',
				value: 'background',
				label: 'Background'
			},
			{
				href: '#program-rollout',
				value: 'programRollout',
				label: 'Program Rollout'
			},
			{
				href: '#impact',
				value: 'impact',
				label: 'Impact'
			},
			{
				href: '#cost',
				value: 'cost',
				label: 'Cost'
			},
			{
				href: '#reasons-for-success',
				value: 'reasonsForSuccess',
				label: 'Reasons For Success'
			},
			{
				href: '#implications',
				value: 'implications',
				label: 'Implications'
			}
		];

		return (
			<div className='case-study-navigation-wrapper' ref='caseStudyNavigationTrigger'>
				<div className='case-study-navigation' ref='caseStudyNavigation'>
					<div className='container--narrow px2 text--center visible--md'>
						{sections.map((section, key) => {
							var activeClass = this.state.active === key ? 'active' : '';
							return (<a href={section.href} key={key} onClick={this.scrollNavigate.bind(this, section.value)} className={`btn px1 py1 ${activeClass}`}>{section.label}</a>);
						})}
					</div>
					<div className='hide--md'>
						<Select searchable={false} clearable={false} name='case-study-navigation' value={sections[0].value} options={sections} onChange={this.scrollNavigateFromSelect.bind(this)} />
					</div>
				</div>
			</div>
		);
	}
}

export default CaseStudyNavigation;
