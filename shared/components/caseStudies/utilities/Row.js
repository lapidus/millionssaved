
import React from 'react';

//
// Case Study Row
//
class Row extends React.Component {
	render() {
		return (
			<div className='mxn1 mx-sm-n2 clearfix body-text'>
				{this.props.children}
			</div>
		);
	}
}

export default Row;
