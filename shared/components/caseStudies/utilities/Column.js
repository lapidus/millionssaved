
import React from 'react';

//
// Case Study Column
//
class Column extends React.Component {
	render() {
		return (
			<div className='col col-md-6 px1 px-sm-2'>
				{this.props.children}
			</div>
		);
	}
}

export default Column;
