
export default function bootstrapSourcing(sups) {
	for (var i = 0; i < sups.length; i++) {
    var a = sups[i].attributes[0].value.split('<a');
    if(a.length > 1) a.unshift('<a target="_blank"');
    sups[i].innerHTML = `[${i+1}]<div class='tooltip'>${a.join('')}</div>`;
	}
}
