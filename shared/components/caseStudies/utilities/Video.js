
import React from 'react';

class Video extends React.Component {
	constructor() {
		super();

		this.activateVideo = this.activateVideo.bind(this);
	}

	activateVideo() {
		var frame = this.refs.frame;
		if(this.props.videoType === 'youTube') {
			frame.innerHTML = `<iframe width="100%" height="600" src="${this.props.videoSource}?autoplay=1" frameBorder="0" allowFullScreen></iframe>`;
		}
		else {
			frame.innerHTML = `<iframe width="100%" height="600" src="${this.props.videoSource}" frameBorder="0" webkitAllowFullScreen mozAllowFullScreen allowFullScreen></iframe>`;
		}
	}

	render() {
		return (
			<div className='iframe-container' ref='frame' onClick={this.activateVideo}>
				<img src={this.props.videoCover ? this.props.videoCover : ''} />
				<div className='play'></div>
			</div>
		);
	}
}

export default Video;
