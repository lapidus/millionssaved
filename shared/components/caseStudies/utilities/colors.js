
export default {
  grey100: '#f6f6f6',
  grey150: '#f2f2f2',
  // grey200: '#e2e2e2',
  grey200: '#E3E0DB',
  grey300: '#DAD7CD',
  grey400: '#C0BDB8',
  grey800: '#302D28',

  textColor: '#333333',

  red500:  '#EB4C38',
  red700:  '#cB2C18',

  blue500: '#2196F3',
  blue700: '#0288D1',

  turquoise500: '#1ABC9C',
  turquoise700: '#16a085',

  green500:  '#00C853',
  green700:  '#00B259',

  orange500: '#FF9800',
  orange700: '#F57C00',

  yellow500: '#FDBA45',
  yellow700: '#EDAA35',

  pink500:   '#E91E63',
  pink700:   '#C2185B',

  beige500:  '#9D8C70',

  teal500: '#296976',
  teal600: '#195966',
  teal700: '#094956',

  white: '#fff'
};
