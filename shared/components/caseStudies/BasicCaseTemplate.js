
import React from 'react';
import ReactDOM from 'react-dom';

// import RelatedCases from './utilities/RelatedCases';
import Paragraph from './utilities/Paragraph';
import MiniMap from './utilities/MiniMap';

import CaseStudyCover from './utilities/CaseStudyCover';
import CaseStudySummary from './utilities/CaseStudySummary';

import bootstrapSourcing from './utilities/sourcing';

class BasicCase extends React.Component {
	render() {
		var caseStudy = this.props.caseStudy;
		var { lead, textOne, quote, textTwo } = caseStudy.basic || {};

		return (
			<div>
				
				<CaseStudyCover caseStudy={caseStudy} caseId={this.props.caseId} />
				
				<CaseStudySummary caseStudy={caseStudy} caseId={this.props.caseId} />

				<div className='container--narrow px1 px-sm-2 pt4' ref='caseContent'>
					<a name='case-content'></a>
					<div className='mxn1 mx-sm-n2 clearfix body-text'>
						<div className='col col-md-12 px1 px-sm-2'>
							<div className='mxn1 mx-sm-n2 clearfix'>
								<div className='col col-md-10 col-md-offset-1 px1 px-sm-2'>
									<Paragraph markup={lead} paragraphClass='lead' />
									<MiniMap region={caseStudy.region} highlightCountries={caseStudy.highlightCountries} />
									<Paragraph markup={textOne} />
								</div>
							</div>
							<div className='mxn1 mx-sm-n2 clearfix'>
								<div className='col col-md-8 col-md-offset-2 px1 px-sm-2 mb2'>
									<blockquote className='headline py2'>{quote}</blockquote>
								</div>
							</div>
							<div className='mxn1 mx-sm-n2 clearfix'>
								<div className='col col-md-10 col-md-offset-1 px1 px-sm-2'>
									<Paragraph markup={textTwo} />
								</div>
							</div>
						</div>
					</div>
				</div>

				{ /* <RelatedCases previousCase='Previous Case' nextCase='Next Case' /> */ }

			</div>
		);
	}
}

export default BasicCase;
