
import Case1 from './MenAfriVac.caseStudy';
import Case2 from './BotswanaARV.caseStudy';
import Case3 from './ArgentinaPlanNacer.caseStudy';
import Case4 from './ThailandUCS.caseStudy';
import Case5 from './KenyaOVC.caseStudy';
import Case6 from './SouthAfricaCSG.caseStudy';
import Case7 from './VietnamHelmets.caseStudy';
import Case8 from './IndonesiaSanitation.caseStudy';
import Case9 from './ThailandTobacco.caseStudy';
import Case10 from './HaitiPolio.caseStudy';

//
// List all Case Studies in template order
//
// Check order here:
// https://docs.google.com/spreadsheets/d/1Zc4GZW_7iqNWnHDaZGEKEFEphPYSo6mK-AUdCuv2EwQ/edit#gid=1637120184
//
export default [
	Case1,
	Case2,
	Case3,
	Case4,
	Case5,
	Case6,
	Case7,
	Case8,
	Case9,
	Case10
];
