
import React from 'react';
import d3 from 'd3';
import { connect } from 'react-redux';

import colors from '../utilities/colors';

import Select from 'react-select';

import { changeYear, changeDataset } from '../../../actions/kenyaVisualizationActions';

var buttonStyle = {
  default: {
    color: 'inherit',
    background: 'transparent'
  },
  active: {
    color: colors.white,
    background: colors.yellow500
  }
};

class Visualization extends React.Component {
  constructor() {
    super();
    this.updateVisualization = this.updateVisualization.bind(this);
    this.renderVisualization = this.renderVisualization.bind(this);
    this.makeHumans = this.makeHumans.bind(this);
    this.updateYear = this.updateYear.bind(this);
    this.updateDataset = this.updateDataset.bind(this);

    this.visualizationConfig = {};

    this.showSource = this.showSource.bind(this);
    this.hideSource = this.hideSource.bind(this);
  }

  makeHumans(count) {

    var madeHumans = [];

    for (var i = 0; i < count; i++) {
      madeHumans.push({ type: 'positive', iterator: i });
    }

    for (var j = 0; j < (100 - count); j++) {
      madeHumans.push({ type: 'negative', iterator: j });
    }

    return madeHumans;
  }

  renderVisualization() {
    var width = this.visualizationConfig.visualizationWrapperElement.clientWidth,
        height = 640;

    var wrapper = d3.select(this.visualizationConfig.visualizationWrapperElement);

    var groupWidth = width / 2 - 32;
    var colWidth = groupWidth / 2 - 20;
    var humanWidth = colWidth / 10;

    this.visualizationConfig.colWidth = colWidth;
    this.visualizationConfig.humanWidth = humanWidth;

    var container = wrapper
                      .append('div')
                      .attr('id', 'histogram-container')
                      .attr('class', 'histogram clearfix');

    var treatmentContainer = container
                              .append('div')
                              .style('height', '395px')
                              .attr('class', 'histogram__treatment');

    var controlContainer = container
                            .append('div')
                            .style('height', '395px')
                            .attr('class', 'histogram__control');

    treatmentContainer
      .append('div')
      .style({position: 'absolute',top: '100%',left: 0,width:'100%','text-align': 'center'})
      .html('<p class="pt2 m0"><strong>Received cash transfer</strong></p>');

    var treatmentMsgContainer = treatmentContainer
                                  .append('div')
                                  .style({position: 'absolute',top: '100%',left: 0,width:'100%','text-align': 'center', 'padding-right': '3.2rem'})
                                  .append('div')
                                    .style({position: 'relative', 'font-size': '1.4rem'});

    var treatmentPro = treatmentMsgContainer
                                  .append('div')
                                  .style({position: 'absolute',top: 0,right: 0,width:'50%','text-align': 'center', 'padding-left': '3.2rem'})
                                  .html(`<p>${this.props.dataset[this.props.currentDataset].pro}</p>`);

    var treatmentCon = treatmentMsgContainer
                                  .append('div')
                                  .style({position: 'absolute',top: 0,left: 0,width:'50%','text-align': 'center', 'padding-right': '1.4rem'})
                                  .html(`<p>${this.props.dataset[this.props.currentDataset].con}</p>`);

    var treatmentCounter = treatmentContainer
                            .append('div')
                            .style({position: 'absolute',top: '10px',left: '249px'})
                            .html(`<p class='headline'>${this.props.dataset[this.props.currentDataset].treatment[this.props.currentYear]}%</p>`);

    this.visualizationConfig.treatmentContainer = treatmentContainer;
    this.visualizationConfig.treatmentPro = treatmentPro;
    this.visualizationConfig.treatmentCon = treatmentCon;
    this.visualizationConfig.treatmentCounter = treatmentCounter;

    controlContainer
      .append('div')
      .style({position: 'absolute',top: '100%',left: 0,width:'100%','text-align': 'center'})
      .html('<p class="pt2 m0"><strong>Didn\'t receive cash transfer</strong></p>');

    var controlMsgContainer = controlContainer
                                .append('div')
                                .style({position: 'absolute',top: '100%',left: 0,width:'100%','text-align': 'center', 'padding-left': '3.2rem'})
                                .append('div')
                                  .style({position: 'relative', 'font-size': '1.4rem'});

    var controlPro = controlMsgContainer
                      .append('div')
                      .style({position: 'absolute',top: 0,right: 0,width:'50%','text-align': 'center', 'padding-left': '3.2rem'})
                      .html(`<p>${this.props.dataset[this.props.currentDataset].pro}</p>`);

    var controlCon = controlMsgContainer
                      .append('div')
                      .style({position: 'absolute',top: 0,left: 0,width:'50%','text-align': 'center', 'padding-right': '1.4rem'})
                      .html(`<p>${this.props.dataset[this.props.currentDataset].con}</p>`);

    var controlCounter = controlContainer
                          .append('div')
                          .style({position: 'absolute',top: '10px',left: '279px'})
                          .html(`<p class='headline'>${this.props.dataset[this.props.currentDataset].control[this.props.currentYear]}%</p>`);

    this.visualizationConfig.controlContainer = controlContainer;
    this.visualizationConfig.controlPro = controlPro;
    this.visualizationConfig.controlCon = controlCon;
    this.visualizationConfig.controlCounter = controlCounter;

    var treatmentData = this.makeHumans(this.props.dataset[this.props.currentDataset].treatment[this.props.currentYear]);
    var controlData = this.makeHumans(this.props.dataset[this.props.currentDataset].control[this.props.currentYear]);

    var treatmentHumans, controlHumans;

    function transformCalculator(d, i) {
      var iterator1 = parseInt(String(d.iterator).slice(-1));
      var iterator2 = Math.floor(d.iterator / 10) + 1;
      var x = d.type === 'negative' ? iterator1 * humanWidth + 'px' : iterator1 * humanWidth + colWidth + 40 + 'px';
      var y = (395 - iterator2 * 31) + 'px';
      return 'translate(' + x + ', ' + y + ')';
    }

    function renderHumans(data1, data2) {

      var humanVector = 'M13.532,6.68h-3.063C9.131,6.68,8,7.875,8,9.289v6.53c0,0.134,0.053,0.261,0.146,0.354L9,17.027v6.152c0,0.275,0.224,0.5,0.5,0.5h5c0.275,0,0.5-0.225,0.5-0.5v-6.152l0.854-0.854C15.948,16.08,16,15.953,16,15.819v-6.53C16.002,7.875,14.871,6.68,13.532,6.68z';

      treatmentContainer.selectAll('.human')
        .data(data1).enter()
        .append('div').attr('class', 'human')
        .style('width', (colWidth / 10) + 'px')
        .style('height', '30px')
        .style('-webkit-transform', transformCalculator)
        .style('-moz-transform', transformCalculator)
        .style('-ms-transform', transformCalculator)
        .style('-o-transform', transformCalculator)
        .style('transform', transformCalculator)
        .style('fill', function(d) {
          // return d.type === 'positive' ? '#204F5A' : '#DAD7CD';
          return d.type === 'positive' ? colors.teal500 : colors.grey400;
        })
        .append('svg')
          .append('g')
          .attr('transform', 'scale(1.1)')
          .each(function() {
            var el = d3.select(this);
            el.append('path').attr('d', humanVector);
            el.append('circle').attr('cx', 12.001).attr('cy', 3.32).attr('r', 3);
          });

      controlContainer.selectAll('.human')
        .data(data2).enter()
        .append('div').attr('class', 'human')
        .style('width', (colWidth / 10) + 'px')
        .style('height', '30px')
        .style('-webkit-transform', transformCalculator)
        .style('-moz-transform', transformCalculator)
        .style('-ms-transform', transformCalculator)
        .style('-o-transform', transformCalculator)
        .style('transform', transformCalculator)
        .style('fill', function(d) {
          // return d.type === 'positive' ? '#204F5A' : '#DAD7CD';
          return d.type === 'positive' ? colors.teal500 : colors.grey400;
        })
        .append('svg')
          .append('g')
          .attr('transform', 'scale(1.1)')
          .each(function() {
            var el = d3.select(this);
            el.append('path').attr('d', humanVector);
            el.append('circle').attr('cx', 12.001).attr('cy', 3.32).attr('r', 3);
          });
    }

    renderHumans(treatmentData, controlData);
  }

  updateVisualization(count) {
    var humanWidth = this.visualizationConfig.humanWidth;
    var colWidth = this.visualizationConfig.colWidth;

    function transformCalculator(d, i) {
      var iterator1 = parseInt(String(d.iterator).slice(-1));
      var iterator2 = Math.floor(d.iterator / 10) + 1;
      var x = d.type === 'negative' ? iterator1 * humanWidth + 'px' : iterator1 * humanWidth + colWidth + 40 + 'px';
      var y = (395 - iterator2 * 31) + 'px';
      return 'translate(' + x + ', ' + y + ')';
    }

    var t = this.makeHumans(count[0]) || this.makeHumans(Math.floor(Math.random() * (100 - 0)) + 0);
    var c = this.makeHumans(count[1]) || this.makeHumans(Math.floor(Math.random() * (100 - 0)) + 0);

    this.visualizationConfig.treatmentContainer.selectAll('.human')
      .data(t)
      .transition()
      .style('fill', function(d) {
        // return d.type === 'positive' ? '#204F5A' : '#DAD7CD';
        return d.type === 'positive' ? colors.teal500 : colors.grey400;
      })
      .style('-webkit-transform', transformCalculator)
      .style('-moz-transform', transformCalculator)
      .style('-ms-transform', transformCalculator)
      .style('-o-transform', transformCalculator)
      .style('transform', transformCalculator)
      .duration(0)
      .delay(function(d, i) {
        return i * 10;
      });

    this.visualizationConfig.treatmentCounter
        .html(`<p class='headline'>${count[0]}%</p>`);

    this.visualizationConfig.controlContainer.selectAll('.human')
      .data(c)
      .transition()
      .style('fill', function(d) {
        // return d.type === 'positive' ? '#204F5A' : '#DAD7CD';
        return d.type === 'positive' ? colors.teal500 : colors.grey400;
      })
      .style('-webkit-transform', transformCalculator)
      .style('-moz-transform', transformCalculator)
      .style('-ms-transform', transformCalculator)
      .style('-o-transform', transformCalculator)
      .style('transform', transformCalculator)
      .duration(0)
      .delay(function(d, i) {
        return i * 10;
      });

    this.visualizationConfig.controlCounter
      .html(`<p class='headline'>${count[1]}%</p>`);

    this.visualizationConfig.treatmentPro.html(`<p>${this.props.dataset[this.props.currentDataset].pro}</p>`);
    this.visualizationConfig.treatmentCon.html(`<p>${this.props.dataset[this.props.currentDataset].con}</p>`);
    this.visualizationConfig.controlPro.html(`<p>${this.props.dataset[this.props.currentDataset].pro}</p>`);
    this.visualizationConfig.controlCon.html(`<p>${this.props.dataset[this.props.currentDataset].con}</p>`);

  }

	componentDidMount() {
    var el = this.refs.mainVisualizationContainer;
		var wrapper = d3.select(el);
    this.visualizationConfig.visualizationWrapperElement = el;

    this.renderVisualization();
  }

  updateDataset(val) {
    // var datasetSelector = this.refs.datasetSelector;

    this.props.dispatch(changeDataset(val));
  }

  updateYear(newYear) {
    var before = this.refs.before;
    var after = this.refs.after;

    this.props.dispatch(changeYear(newYear));
  }

  componentDidUpdate(prevProps) {
    this.updateVisualization([this.props.dataset[this.props.currentDataset].treatment[this.props.currentYear], this.props.dataset[this.props.currentDataset].control[this.props.currentYear]]);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  showSource() {
    var source = this.refs.source;
    source.style.display = 'block';
    Velocity(source, {
      opacity: [ 1, 0 ]
    }, {
      duration: 500
    });
  }

  hideSource() {
    var source = this.refs.source;
    Velocity(source, {
      opacity: [ 0, 1 ]
    }, {
      duration: 500,
      complete: () => {
        source.style.display = 'none';
      }
    });
  }

	render() {

    var select = [
      { value: 0, label: 'Households consuming meat in the past 7 days' },
      { value: 1, label: 'Households consuming milk in last 7 days' },
      { value: 2, label: 'Households consuming fruit in last 7 days' },
      { value: 3, label: 'Households living on less than $1/day' },
      { value: 4, label: 'Households where kids aged 6-17 ever attended school' },
      { value: 5, label: 'Households where teens aged 14-17 years enrolled in secondary school' }
    ];

		return (
			<div className='bg--grey mb2' style={{position: 'relative',paddingTop: '14rem', paddingBottom: '10rem'}}>
				<div className='toolbar--top'>
					<div className='container--narrow px1 px-sm-2 pt2'>
						<h2 className='headline mt0 mb1' onClick={this.updateVisualization}>Impact of the Kenya Program</h2>
            <div className='mxn2'>
              <div className='col col-sm-6 px1 px-sm-2'>
                <Select searchable={false} clearable={false} name='kenyaOVC-selector' value={this.props.currentDataset} options={select} onChange={this.updateDataset} />
              </div>
              <div className='col col-sm-6 px1 px-sm-2 text--right'>
                <button className='btn px1' style={buttonStyle[this.props.currentYear == 0 ? 'active' : 'default']} onClick={() => this.updateYear(0)} ref='before'>2007</button>
                <button className='btn px1' style={buttonStyle[this.props.currentYear == 1 ? 'active' : 'default']} onClick={() => this.updateYear(1)} ref='after'>2009</button>
              </div>
            </div>
					</div>
				</div>
				<div className='container--narrow px1 px-sm-2 pt1' style={{overflow: 'hidden',paddingBottom:'6.4rem'}}>
					<div className='main-visualization' ref='mainVisualizationContainer'></div>
				</div>

        <div className='main-visualization-source' ref='source'>
          <div className='source-content'>
            <div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
            <button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
          </div>
        </div>

        <div className='toolbar--bottom'>
          <div className='container--narrow px2 py1 clearfix'>
            <div className='col col-sm-12 text--right'>
              <button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
            </div>
          </div>
        </div>
			</div>
		);
	}
}

function mapStateToProps(state) {
  return {
    dataset: state.kenyaVisualizationReducer.datasets,
    currentDataset: state.kenyaVisualizationReducer.currentDataset,
    currentYear: state.kenyaVisualizationReducer.currentYear,
    dispatch: state.kenyaVisualizationReducer.dispatch
  };
}

export default connect(mapStateToProps)(Visualization);
