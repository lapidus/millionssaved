
import React from 'react';
import d3 from 'd3';
import d3Tip from 'd3-tip';
import topojson from 'topojson';
import findWhere from 'lodash/collection/findWhere';

import colors from '../utilities/colors';

import { connect } from 'react-redux';
import { changeYear, changeDataset } from '../../../actions/menAfriVacVisualizationActions';

import Select from 'react-select';

var buttonStyle = {
  default: {
    color: 'inherit',
    background: 'transparent'
  },
  active: {
    color: colors.textColor,
    background: colors.yellow500
  }
};

class Visualization extends React.Component {
	constructor() {
		super();
		this.updateYear = this.updateYear.bind(this);
		this.updateDataset = this.updateDataset.bind(this);
		this.updateVisualization = this.updateVisualization.bind(this);
		this.showSource = this.showSource.bind(this);
		this.hideSource = this.hideSource.bind(this);

		this.visualizationConfig = {};
	}

	componentDidMount() {

		var el = this.refs.mapWrapper;
		var mapContainer = d3.select(el);

		var width = window.outerWidth,
				height = 640;

		var minColor = '#B89434',
				maxColor = '#EB4C38';

		var dataset = this.props.dataset;
		var currentDataset = this.props.currentDataset;
		var currentYear = this.props.currentYear;

		this.visualizationConfig.bubbleScale = {};

		this.visualizationConfig.bubbleScale.cases = d3.scale.sqrt()
			.domain([1, 60000])
			.range([1, 70]);

		this.visualizationConfig.bubbleScale.deaths = d3.scale.sqrt()
			.domain([1, 60000])
			.range([1, 70]);

		this.visualizationConfig.tip = d3Tip().attr('class', 'd3-tip').html((d) => {
			return `
				<div class='d3-tip-content text--center'>
					<p class='title m0'>${d.name}</p>
					<p class="m0">${d3.format(',')(d[currentDataset][currentYear]) || 'N/A'}</p>
					${ d.hyperendemic ? `<p class="m0 text--yellow small">Hyperendemic Country</p>` : '' }
				</div>
			`;
		});

		var _self = this;
		var url = window.location.origin + '/files/world-50m.json';

		d3.json(url, function(err, world) {
			if(err) { return; }

			var projection = d3.geo.mercator()
												 .scale(800)
												 .rotate([-10,0,0])
												 .translate([width / 2, height / 1.3]);

			var path = d3.geo.path().projection(projection);

			var svg = mapContainer.append('svg')
									.attr('width', '100%')
									.attr('height', height)
									.attr('viewBox', '0 0 ' + width + ' ' + height)
									.append('g');

			_self.visualizationConfig.map = svg.insert('g')
									 .attr('class', 'world');

			_self.visualizationConfig.map.append('rect')
				 .attr('class', 'overlay')
				 .attr('width', width)
				 .attr('height', height);

			var topojsonShapes = topojson.feature(world, world.objects.countries).features;

			_self.visualizationConfig.map.selectAll('.shape')
				 .data(topojsonShapes)
				 .enter()
				 .append('path')
				 .attr('class', function(d) {
				 		var targetCountry = findWhere(dataset, { id: d.id });
				 		return targetCountry ? 'fill--grey-light shape shape--highlight' : 'fill--grey-light shape';
				 })
				 .attr('d', path)
				 // .style('fill', function(d) {
				 // 		return '#f1f2f3';
				 // })
				 .style('stroke', colors.white)
				 .style('stroke-width', 1);

			_self.visualizationConfig.map.call(_self.visualizationConfig.tip);

			_self.visualizationConfig.bubbles = _self.visualizationConfig.map.selectAll('.bubble')
				 .data(dataset)
				 .enter()
				 .append('circle')
				 .attr('r', function(d) {
				 		return _self.visualizationConfig.bubbleScale[currentDataset](d[currentDataset][currentYear]);
				 })
				 .attr('class', function(d) {
				 		return 'fill--yellow bubble ' + d.name;
				 })
				 .attr('cx', function(d) {
				 		return projection(d.location)[0];
				 })
				 .attr('cy', function(d) {
				 		return projection(d.location)[1];
				 })
				 // .style('fill', '#B89434')
				 .style('stroke', colors.white)
				 .style('stroke-width', 2)
				 .on('mouseover', _self.visualizationConfig.tip.show)
				 .on('mouseout', _self.visualizationConfig.tip.hide);

		});

	}

	updateVisualization() {
		var newYear = this.props.currentYear;
		var newDataset = this.props.currentDataset;
		var bubbleScale = this.visualizationConfig.bubbleScale;

		this.visualizationConfig.tip.html((d) => {
			return `
				<div class='d3-tip-content text--center'>
					<p class='title m0'>${d.name}</p>
					<p class="m0">${d3.format(',')(d[newDataset][newYear]) || 'N/A'}</p>
					${ d.hyperendemic ? `<p class="m0 text--yellow small">Hyperendemic Country</p>` : '' }
				</div>
			`;
		});

		this.visualizationConfig.bubbles.transition()
			.attr('r', function(d) {
				return bubbleScale[newDataset](d[newDataset][newYear]);
			})
			.duration(1000);
		
	}

	updateYear(newYear) {
		this.props.dispatch(changeYear(newYear));
	}

	updateDataset(val) {
		this.props.dispatch(changeDataset(val));
	}

	componentDidUpdate() {
		this.updateVisualization();
	}

	showSource() {
  	var source = this.refs.source;
  	source.style.display = 'block';
  	Velocity(source, {
  		opacity: [ 1, 0 ]
  	}, {
  		duration: 500
  	});
  }

  hideSource() {
  	var source = this.refs.source;
  	Velocity(source, {
  		opacity: [ 0, 1 ]
  	}, {
  		duration: 500,
  		complete: () => {
  			source.style.display = 'none';
  		}
  	});
  }

	shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  componentWillUnmount() {
  	if(this.visualizationConfig.tip) {
  		d3.select('.d3-tip').remove();
  	}
  }

	render() {

		var select = [
			{ value: 'cases', label: 'Number of suspected meningitis cases reported' },
			{ value: 'deaths', label: 'Number of suspected meningitis deaths reported' }
		];

		return (
			<div className='mt1 mb3 main-visualization-container visible--sm'>
				<div className='toolbar--top'>
					<div className='container--narrow px2 pt2'>
						<h2 className='headline mt0 mb1'>Meningitis Across Africa’s Meningitis Belt</h2>
						<div className='toolbar-select'>
							<Select searchable={false} clearable={false} name='menafrivac-selector' value={this.props.currentDataset} options={select} onChange={this.updateDataset} />
						</div>
					</div>
				</div>

				<div className='menAfriVac-map' ref='mapWrapper'></div>

				<div className='main-visualization-source' ref='source'>
					<div className='source-content'>
						<div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
						<button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
					</div>
				</div>
				
				<div className='toolbar--bottom'>
					<div className='container--narrow px2 py1 clearfix'>
						<div className='col col-sm-8'>
							<span className='px1 map-toggle btn' style={buttonStyle[this.props.currentYear == 0 ? 'active' : 'default']} onClick={() => this.updateYear(0)}>2009</span>
							<span className='px1 map-toggle btn' style={buttonStyle[this.props.currentYear == 1 ? 'active' : 'default']} onClick={() => this.updateYear(1)}>2010</span>
							<span className='px1 map-toggle btn' style={buttonStyle[this.props.currentYear == 2 ? 'active' : 'default']} onClick={() => this.updateYear(2)}>2011</span>
							<span className='px1 map-toggle btn' style={buttonStyle[this.props.currentYear == 3 ? 'active' : 'default']} onClick={() => this.updateYear(3)}>2012</span>
							<span className='px1 map-toggle btn' style={buttonStyle[this.props.currentYear == 4 ? 'active' : 'default']} onClick={() => this.updateYear(4)}>2013</span>
							<span className='px1 map-toggle btn' style={buttonStyle[this.props.currentYear == 5 ? 'active' : 'default']} onClick={() => this.updateYear(5)}>2014</span>
						</div>
						<div className='col col-sm-4 text--right'>
							<button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
  return {
    dataset: state.menAfriVacVisualizationReducer.datasets,
    currentDataset: state.menAfriVacVisualizationReducer.currentDataset,
    currentYear: state.menAfriVacVisualizationReducer.currentYear,
    dispatch: state.menAfriVacVisualizationReducer.dispatch
  };
}

export default connect(mapStateToProps)(Visualization);
