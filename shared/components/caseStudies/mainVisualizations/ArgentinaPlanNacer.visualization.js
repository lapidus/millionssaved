
import React from 'react';
import d3 from 'd3';

class Visualization extends React.Component {

	constructor(props) {
		super(props);

		this.showSource = this.showSource.bind(this);
		this.hideSource = this.hideSource.bind(this);
	}

	componentDidMount() {

		var el = this.refs.visualization;
		var visualizationContainer = d3.select(el);

		var dataset = [0.74, 0.22, 0.19, 0.09];
		var startHeight = 200;

		var groups = visualizationContainer.selectAll('.group__bars')
										.data(dataset)
										.each(function(d, i) {
											var _self = d3.select(this);
											_self.append('div')
													 .attr('class', 'bar-pre')
													 .style('height', function() {
													 		return startHeight + 'px';
													 });
											_self.append('div')
													 .attr('class', 'bar-post')
													 .style('height', function() {
													 		return (startHeight - (startHeight * d)) + 'px';
													 })

											var labelContent = '<span class="headline m0"><strong>' + (d * 100) + '%</strong></span><br />';
													labelContent+= '<span>reduction</span>';

											_self.append('div')
													 .attr('class', 'bar-label')
													 .style({
													 		bottom: (startHeight - (startHeight * d)) + 8 + 'px',
													 })
													 .html(labelContent);
										});

	}

	showSource() {
  	var source = this.refs.source;
  	source.style.display = 'block';
  	Velocity(source, {
  		opacity: [ 1, 0 ]
  	}, {
  		duration: 500
  	});
  }

  hideSource() {
  	var source = this.refs.source;
  	Velocity(source, {
  		opacity: [ 0, 1 ]
  	}, {
  		duration: 500,
  		complete: () => {
  			source.style.display = 'none';
  		}
  	});
  }

	render() {
		return (
			<div className='mb3 main-visualization-container bg--grey'>
				<div className='toolbar--top'>
					<div className='container--narrow px1 px-sm-2 pt2'>
						<div className='mx1 mx-sm-n2 clearfix'>
							<div className='col col-md-8 px1 px-sm-2'>
								<h2 className='headline m0'>Plan Nacer Improvements in<br />Neonatal Mortality and Low Birth Weight</h2>
							</div>
							<div className='col col-md-4 px1 px-sm-2 text--right'>
								<span className='title text--muted'>2004 - 2008</span>
							</div>
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 pt2'>
					<div className='visualization--plan-nacer clearfix' ref='visualization'>
						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>Risk of<br />neonatal death</p>
								<p>among beneficiaries at participating hospitals</p>
							</div>
						</div>
						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>Risk of<br />neonatal death</p>
								<p>among non-beneficiaries at participating hospitals</p>
							</div>
						</div>
						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>Risk of low<br />birth weight</p>
								<p>among individual beneficiaries</p>
							</div>
						</div>
						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>Risk of low<br />birth weight</p>
								<p>among non-beneficiaries in participating facilities</p>
							</div>
						</div>
					</div>
				</div>

				<div className='main-visualization-source' ref='source'>
					<div className='source-content'>
						<div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
						<button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
					</div>
				</div>

				<div className='toolbar--bottom'>
					<div className='container--narrow px2 py1 clearfix'>
						<div className='col col-sm-4 col-sm-offset-8 text--right'>
							<button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
						</div>
					</div>
				</div>

			</div>
		);
	}
}

export default Visualization;
