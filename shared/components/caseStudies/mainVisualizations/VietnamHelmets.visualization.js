
import React from 'react';
import d3 from 'd3';

import colors from '../utilities/colors';

import {Motion, spring} from 'react-motion';

var buttonStyle = {
  default: {
    color: 'inherit',
    background: 'transparent'
  },
  active: {
    color: colors.white,
    background: colors.yellow500
  }
};

class Visualization extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      year: 0,
      sortedDataset: () => {
        return this.state.dataset.sort((a,b) => b.percentage[this.state.year] - a.percentage[this.state.year]);
      },
      dataset: [
        {
          id: 0,
          answer: 'Just driving near home',
          percentage: [38.7, 91.2],
          count: [841, 320]
        },
        {
          id: 1,
          answer: 'Uncomfortable, invonvenient',
          percentage: [13.3 , 13.4],
          count: [288, 47]
        },
        {
          id: 2,
          answer: 'Do not see others wearing helmet',
          percentage: [10.2, 4.6],
          count: [222, 16]
        },
        {
          id: 3,
          answer: 'Not required by law',
          percentage: [50.3, 0.3],
          count: [1092, 1]
        },
        {
          id: 4,
          answer: 'No money',
          percentage: [5.7, 1.7],
          count: [124, 6]
        },
        {
          id: 5,
          answer: 'Do not like the look',
          percentage: [0.3, 0],
          count: [7, 0]
        },
        {
          id: 6,
          answer: 'Hard to see/hear',
          percentage: [0.3, 1.1],
          count: [6, 4]
        },
        {
          id: 7,
          answer: 'No helmet',
          percentage: [0.4, 0],
          count: [9, 0]
        },
        {
          id: 8,
          answer: 'No motorbike',
          percentage: [1.3, 0],
          count: [28, 0]
        },
        {
          id: 9,
          answer: 'Motorbike taxi did not have helmet',
          percentage: [0.8, 0],
          count: [18, 0]
        },
        {
          id: 10,
          answer: 'Forgot',
          percentage: [0.1, 0],
          count: [3, 0]
        },
        {
          id: 11,
          answer: 'Do not understand, is not necessary',
          percentage: [0.5, 0],
          count: [10, 0]
        },
        {
          id: 12,
          answer: 'No police, small fine',
          percentage: [0.2, 0],
          count: [5, 0]
        },
        {
          id: 13,
          answer: 'Travel at night',
          percentage: [0.1, 0],
          count: [2, 0]
        },
        {
          id: 14,
          answer: 'Other',
          percentage: [2.2, 7.1],
          count: [47, 25]
        }
      ]
    };

    this.updateYear = this.updateYear.bind(this);
    this.updateVisualization = this.updateVisualization.bind(this);

    this.showSource = this.showSource.bind(this);
    this.hideSource = this.hideSource.bind(this);
  }

  updateYear(year) {
    // console.log('Updated year to: ', year);
    this.setState({
      year: year
    });
  }

  updateVisualization() {
    // console.log('Updating visualization');
  }

  showSource() {
    var source = this.refs.source;
    source.style.display = 'block';
    Velocity(source, {
      opacity: [ 1, 0 ]
    }, {
      duration: 500
    });
  }

  hideSource() {
    var source = this.refs.source;
    Velocity(source, {
      opacity: [ 0, 1 ]
    }, {
      duration: 500,
      complete: () => {
        source.style.display = 'none';
      }
    });
  }

	render() {

		return (
			<div className='bg--grey mb2' style={{position: 'relative',paddingTop: '14rem', paddingBottom: '10rem'}}>
        <div className='toolbar--top'>
          <div className='container--narrow px1 px-sm-2 pt2'>
            <h2 className='headline mt0 mb1'>Reasons For Not Wearing A Helmet</h2>
            { /* <div className='toolbar-select'>
              <Select searchable={false} clearable={false} name='kenyaOVC-selector' value={this.props.currentDataset} options={select} onChange={this.updateDataset} />
            </div> */ }
          </div>
        </div>
        <div className='container--narrow px1 px-sm-2 pt1 pb2' style={{overflow: 'hidden'}}>
          <div className='main-visualization' ref='mainVisualizationContainer' style={{position: 'relative', height: `${this.state.dataset.length * 33}px`}}>
            {this.state.sortedDataset().map((item, i) => {
              return (
                <Motion key={item.id} style={{top: spring(i*33, {stiffness: 160, damping: 25}), width: spring(item.percentage[this.state.year], {stiffness: 160, damping: 25})}}>
                  {val => <VisualizationListItem item={item} year={this.state.year} springVal={val} />}
                </Motion> 
              )
            })}
          </div>
        </div>

        <div className='main-visualization-source' ref='source'>
          <div className='source-content'>
            <div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
            <button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
          </div>
        </div>

        <div className='toolbar--bottom'>
          <div className='container--narrow px2 py1 clearfix'>
            <div className='col col-sm-8'>
              <button className='btn px1' style={buttonStyle[this.state.year == 0 ? 'active' : 'default']} onClick={() => this.updateYear(0)} ref='before'>Before</button>
              <button className='btn px1' style={buttonStyle[this.state.year == 1 ? 'active' : 'default']} onClick={() => this.updateYear(1)} ref='after'>After</button>
            </div>
            <div className='col col-sm-4 text--right'>
              <button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
            </div>
          </div>
        </div>

      </div>
		);
	}
}

class VisualizationListItem extends React.Component {
  render() {
    return (
      <div className='clearfix' style={{marginBottom: '8px', width: '100%', position: 'absolute', transform: `translateY(${this.props.springVal.top}px)`}}>
        <div className="col col-md-4 text--right" style={{color: this.props.item.id === 3 ? colors.yellow500 : 'inherit'}}>
          {this.props.item.answer}
        </div>
        <div className="col col-md-7">
          <div style={{position: 'relative', height: '25px', background: colors.grey200, marginLeft: '20px', marginRight: '20px'}}>
            <div style={{
              position: 'absolute',
              height: '25px',
              width: `${this.props.springVal.width}%`,
              background: colors.yellow500}}>
            </div>
          </div>
        </div>
        <div className="col col-md-1">{this.props.item.percentage[this.props.year]}%</div>
      </div>
    );
  }
}

export default Visualization;
