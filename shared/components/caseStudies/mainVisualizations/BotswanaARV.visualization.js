
import React from 'react';
import d3 from 'd3';
import d3Tip from 'd3-tip';

import colors from '../utilities/colors';

class Visualization extends React.Component {

  constructor(props) {
    super(props);

    this.showSource = this.showSource.bind(this);
    this.hideSource = this.hideSource.bind(this);
  }

	componentDidMount() {

		// var el = this.refs.visualization.getDOMNode();
		var el = this.refs.visualization;
		var visualizationContainer = d3.select(el);

		var width = el.clientWidth,
				height = 560,
				margins = { x: 40, y: 100 };

		var dataset = [
			{ year: 1991, deaths: 1500, min: 1300, max: 1800   },
			{ year: 1992, deaths: 2200, min: 1900, max: 2800   },
			{ year: 1993, deaths: 3100, min: 2800, max: 4100   },
			{ year: 1994, deaths: 4400, min: 3900, max: 5800   },
			{ year: 1995, deaths: 5900, min: 5200, max: 7800   },
			{ year: 1996, deaths: 7600, min: 6700, max: 10000  },
			{ year: 1997, deaths: 9600, min: 8400, max: 13000  },
			{ year: 1998, deaths: 12000, min: 10000, max: 16000 },
			{ year: 1999, deaths: 14000, min: 12000, max: 19000 },
			{ year: 2000, deaths: 16000, min: 14000, max: 22000 },
			{ year: 2001, deaths: 18000, min: 15000, max: 25000 },
			{ year: 2002, deaths: 19000, min: 16000, max: 28000 },
			{ year: 2003, deaths: 19000, min: 16000, max: 28000 },
			{ year: 2004, deaths: 17000, min: 14000, max: 26000 },
			{ year: 2005, deaths: 13000, min: 11000, max: 21000 },
			{ year: 2006, deaths: 10000, min: 8500, max: 18000  },
			{ year: 2007, deaths: 9100, min: 7400, max: 16000  },
			{ year: 2008, deaths: 9400, min: 8000, max: 14000  },
			{ year: 2009, deaths: 8500, min: 7000, max: 14000  },
			{ year: 2010, deaths: 7100, min: 5700, max: 12000  },
			{ year: 2011, deaths: 6400, min: 5100, max: 11000  },
			{ year: 2012, deaths: 6100, min: 4900, max: 7800   },
			{ year: 2013, deaths: 5800, min: 4500, max: 7600   },
			{ year: 2014, deaths: 5100, min: 3800, max: 6900   }
		];

		var xScale = d3.scale.linear()
								   .domain([1990, 2015])
								   .range([margins.x, width - 2]);

		var yScale = d3.scale.linear()
									 .domain([0, 28])
								   .range([height - margins.y, margins.y]);

		var svg = visualizationContainer.append('svg')
								.attr('width', width)
								.attr('height', height)
								.attr('viewBox', `0 0 ${width} ${height}`);

		var xAxis = d3.svg.axis()
							    .scale(xScale)
							    .tickFormat(d3.format('d'));
  
		var yAxis = d3.svg.axis()
    					    .scale(yScale)
    					    .orient('left');

    svg.append('g')
    	 .attr('class', 'axis x-axis')
    	 .attr('transform', 'translate(0, ' + (height - margins.y) + ')')
    	 .call(xAxis);

    svg.append('g')
    	 .attr('class', 'axis y-axis')
    	 .attr('transform', 'translate(' + margins.x + ', 0)')
    	 .call(yAxis);

    var lineGenerator = d3.svg.line()
    											.x(function(d) {
    												return xScale(d.year);
    											})
    											.y(function(d) {
    												return yScale(d.deaths / 1000);
    											});

   //  var maxLineGenerator = d3.svg.line()
	  //   											.x(function(d) {
	  //   												return xScale(d.year);
	  //   											})
	  //   											.y(function(d) {
	  //   												return yScale(d.max / 1000);
	  //   											});

	  // var minLineGenerator = d3.svg.line()
	  //   											.x(function(d) {
	  //   												return xScale(d.year);
	  //   											})
	  //   											.y(function(d) {
	  //   												return yScale(d.min / 1000);
	  //   											});

    function createDeviationPath() {
    	var path = `M${xScale(dataset[0].year)},${yScale(dataset[0].max / 1000)} `;

    	for (var i = 1 ; i <= dataset.length - 1; i++) {
    		path += `L${xScale(dataset[i].year)},${yScale(dataset[i].max / 1000)} `;
    	}

    	for (var j = dataset.length - 1; j >= 0; j--) {
    		path += `L${xScale(dataset[j].year)},${yScale(dataset[j].min / 1000)} `;
    	}

    	return path + 'Z';
    }

    var deviationArea = svg.append('path')
    								.attr('d', createDeviationPath())
    								.attr('fill', colors.grey200);

    svg.append('path')
    	 .attr('d', lineGenerator(dataset))
    	 .attr('stroke', colors.grey100)
    	 .attr('stroke-width', 6)
    	 .attr('fill', 'none');							

    svg.append('path')
    	 .attr('d', lineGenerator(dataset))
    	 // .attr('stroke', '#B89434')
       .attr('stroke', colors.yellow500)
    	 .attr('stroke-width', 2)
    	 .attr('fill', 'none');

    // svg.append('path')
    // 	 .attr('d', minLineGenerator(dataset))
    // 	 .attr('stroke', '#D0CDC8')
    // 	 .attr('stroke-width', 2)
    // 	 .attr('fill', 'none');
    // svg.append('path')
    // 	 .attr('d', maxLineGenerator(dataset))
    // 	 .attr('stroke', '#D0CDC8')
    // 	 .attr('stroke-width', 2)
    // 	 .attr('fill', 'none');

    svg.append('rect')
			 .attr('x', xScale(2002) - 3)
			 .attr('y', margins.y-10)
			 .attr('width', 6)
			 .attr('height', (height + 11 - 2*margins.y))
			 .attr('fill', colors.grey100);

    svg.append('rect')
			 .attr('x', xScale(2002) - 1)
			 .attr('y', margins.y-10)
			 .attr('width', 2)
			 .attr('height', (height + 11 - 2*margins.y))
			 // .attr('fill', '#204F5A');
       .attr('fill', colors.teal500);

	  svg.append('text')
	  	.attr('x', xScale(2002) - 20)
	  	.attr('y', margins.y - 20)
			.text('Masa Rollout');

		var tip = d3Tip().attr('class', 'd3-tip').html((d) => {
			return `
				<div class='d3-tip-content text--center'>
					<p class='title m0'>${d.year}</p>
					<p class="m0">${d.deaths} deaths</p>
					<p class="m0"><small>[${d.min} - ${d.max}]</small></p>
				</div>
			`;
		});

	  svg.selectAll('.points-of-interest')
    	 .data(dataset)
    	 .enter()
    	 .append('circle')
    	 	.attr('class', 'points-of-interest')
    	 	.attr('cx', function(d) {
    	 		return xScale(d.year);
    	 	})
    	 	.attr('cy', function(d) {
    	 		return yScale(d.deaths / 1000);
    	 	})
    	 	.attr('r', 6)
    	 	.attr('fill', colors.yellow500)
    	 	.attr('stroke', colors.grey100)
    	 	.attr('stroke-width', 2)
    	 	.on('mouseover', tip.show)
    	 	.on('mouseout', tip.hide);

		svg.call(tip);

	}

  showSource() {
    var source = this.refs.source;
    source.style.display = 'block';
    Velocity(source, {
      opacity: [ 1, 0 ]
    }, {
      duration: 500
    });
  }

  hideSource() {
    var source = this.refs.source;
    Velocity(source, {
      opacity: [ 0, 1 ]
    }, {
      duration: 500,
      complete: () => {
        source.style.display = 'none';
      }
    });
  }

	componentWillUnmount() {
		d3.select('.d3-tip').remove();
	}

	render() {
		return (
			<div className='main-visualization-container bg--grey mb3'>
				<div className='toolbar--top'>
					<div className='container--narrow px1 px-sm-2 pt2'>
						<div className='mxn1 mx-sm-n2 clearfix'>
							<div className='col col-md-8 px1 px-sm-2'>
								<h2 className='headline m0'>AIDS related deaths in Botswana<br /><span className='text--yellow'>(in thousands)</span></h2>
							</div>
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 pt4'>
					<div className='visualization--line-chart' ref='visualization'></div>
				</div>

        <div className='main-visualization-source' ref='source'>
          <div className='source-content'>
            <div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
            <button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
          </div>
        </div>

        <div className='toolbar--bottom'>
          <div className='container--narrow px2 py1 clearfix'>
            <div className='col col-sm-4 col-sm-offset-8 text--right'>
              <button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
            </div>
          </div>
        </div>

			</div>
		);
	}
}

export default Visualization;
