
import React from 'react';
import d3 from 'd3';
import d3Tip from 'd3-tip';

import findWhere from 'lodash/collection/findWhere';

class Visualization extends React.Component {

  constructor(props) {
    super(props);

    this.showSource = this.showSource.bind(this);
    this.hideSource = this.hideSource.bind(this);
  }

	componentDidMount() {

		var el = this.refs.visualization;
		var visualizationContainer = d3.select(el);

		var width = el.clientWidth,
				height = 560,
				margins = { x: 40, y: 100 };

		var dataset = [
			{ year: 1998, grant: 'R100', beneficiaries: 21997 },
			{ year: 1999, grant: 'R100', beneficiaries: 150366 },
			{ year: 2000, grant: 'R100', beneficiaries: 1111612 },
			{ year: 2001, grant: 'R110', beneficiaries: 1277396 },
			{ year: 2002, grant: 'R140', beneficiaries: 1998936 },
			{ year: 2003, grant: 'R160', beneficiaries: 2996936 },
			{ year: 2004, grant: 'R170', beneficiaries: 4165545 },
			{ year: 2005, grant: 'R180', beneficiaries: 5913719, age: 'Age raised to 14.' },
			{ year: 2006, grant: 'R190', beneficiaries: 7410760 },
			{ year: 2007, grant: 'R200', beneficiaries: 7975847 },
			{ year: 2008, grant: 'R230', beneficiaries: 8289787, age: 'Age raised to 15.' },
			{ year: 2009, grant: 'R240', beneficiaries: 9071862 },
			{ year: 2010, grant: 'R250', beneficiaries: 10371950, age: 'Age raised to 18.' },
			{ year: 2011, grant: 'R260', beneficiaries: 10373613 },
			{ year: 2012, grant: 'R280', beneficiaries: 10927731 }
		];

		var coverageMilestones = [
			{ year: 2003, coverage: '30%', y: 11000000 },
			{ year: 2006, coverage: '45%', y: 11000000 }
		];

		var xScale = d3.scale.linear()
								   .domain([1997, 2013])
								   .range([margins.x, width - 2]);

		var yScale = d3.scale.linear()
									 .domain([0, 11000000])
								   .range([height - margins.y, margins.y]);

		var svg = visualizationContainer.append('svg')
								.attr('width', width)
								.attr('height', height)
								.attr('viewBox', `0 0 ${width} ${height}`);

		var xAxis = d3.svg.axis()
							    .scale(xScale)
							    .tickFormat((d) => d3.format('d')(d));
  
		var yAxis = d3.svg.axis()
    					    .scale(yScale)
    					    .orient('left')
    					    .tickFormat((d) => d > 0 ? d / 1000000 + 'M' : 0);

    svg.append('g')
    	 .attr('class', 'axis x-axis')
    	 .attr('transform', 'translate(0, ' + (height - margins.y) + ')')
    	 .call(xAxis);

    var xTicks = svg.selectAll('.x-axis .tick')
										.append('text')
											.attr('y', 40)
											.attr('x', -18)
											.attr('font-size', '75%')
											.text((n) => {
												var item = findWhere(dataset, { year: n });
												return `(${item.grant})`;
											});

    svg.append('g')
    	 .attr('class', 'axis y-axis')
    	 .attr('transform', 'translate(' + margins.x + ', 0)')
    	 .call(yAxis);

    var lineGenerator = d3.svg.line()
    											.x((d) => xScale(d.year))
    											.y((d) => yScale(d.beneficiaries));

    svg.append('path')
    	 .attr('d', lineGenerator(dataset))
    	 .attr('stroke', '#f6f6f6')
    	 .attr('stroke-width', 6)
    	 .attr('fill', 'none');							

    svg.append('path')
    	 .attr('d', lineGenerator(dataset))
    	 .attr('stroke', '#B89434')
    	 .attr('stroke-width', 2)
    	 .attr('fill', 'none');

    this.tip = d3Tip().attr('class', 'd3-tip').html((d) => {
			return `
				<div class='d3-tip-content text--center'>
					<p class='title m0'>${d.year}</p>
					<p class="m0">${d3.format(',')(d.beneficiaries)} child beneficiaries</p>
					${ d.age ? `<p class="m0 text--yellow">${d.age}</p>` : '' }
					<p class="m0 small">${d.grant} grant amount/month</p>
				</div>
			`;
		});

    svg.selectAll('.coverage-milestones-shadow')
			.data(coverageMilestones)
			.enter()
			.append('rect')
				.attr('class', 'coverage-milestones-shadow')
				.attr('x', (d) => xScale(d.year) - 3)
				.attr('y', (d) => Math.ceil(yScale(d.y)))
				.attr('width', 6)
				.attr('height', (d) => Math.ceil(height - yScale(d.y) - margins.y))
				.attr('fill', '#f6f6f6');

		svg.selectAll('.coverage-milestones')
			.data(coverageMilestones)
			.enter()
			.append('rect')
				.attr('class', 'coverage-milestones')
				.attr('x', (d) => xScale(d.year) - 1)
				.attr('y', (d) => Math.ceil(yScale(d.y)))
				.attr('width', 2)
				.attr('height', (d) => Math.ceil(height - yScale(d.y) - margins.y))
				.attr('fill', '#C0BDB8');

		svg.selectAll('.coverage-milestones-label')
			.data(coverageMilestones)
			.enter()
			.append('text')
				.attr('class', 'coverage-milestones-label')
				.attr('x', (d) => xScale(d.year) - 10)
				.attr('y', (d) => Math.ceil(yScale(d.y) - 16))
				.text((d) => d.coverage + ' coverage');

	  svg.selectAll('.points-of-interest')
    	 .data(dataset)
    	 .enter()
    	 .append('circle')
    	 	.attr('class', 'points-of-interest')
    	 	.attr('cx', (d) => xScale(d.year))
    	 	.attr('cy', (d) => yScale(d.beneficiaries))
    	 	.attr('r', (d) => d.age ? 8 : 6)
    	 	.attr('fill', '#B89434')
    	 	.attr('stroke', '#f6f6f6')
    	 	.attr('stroke-width', 2)
    	 	.on('mouseover', this.tip.show)
    	 	.on('mouseout', this.tip.hide);

		svg.call(this.tip);

	}

  showSource() {
    var source = this.refs.source;
    source.style.display = 'block';
    Velocity(source, {
      opacity: [ 1, 0 ]
    }, {
      duration: 500
    });
  }

  hideSource() {
    var source = this.refs.source;
    Velocity(source, {
      opacity: [ 0, 1 ]
    }, {
      duration: 500,
      complete: () => {
        source.style.display = 'none';
      }
    });
  }

	componentWillUnmount() {
		if(this.tip) {
			d3.select('.d3-tip').remove();
		}
	}

	render() {
		return (
			<div className='main-visualization-container bg--grey mb3'>
				<div className='toolbar--top'>
					<div className='container--narrow px1 px-sm-2 pt2'>
						<div className='mxn1 mx-sm-n2 clearfix'>
							<div className='col col-md-8 px1 px-sm-2'>
								<h2 className='headline m0'>Child Welfare Grant<br />Policy Shifts with Impact on enrollment</h2>
							</div>
							<div className='col col-md-4 px1 px-sm-2 text--right'>
								<span className='title text--muted'>1998-2012</span>
							</div>
						</div>
					</div>
				</div>

				<div className='container--narrow px1 px-sm-2 pt4'>
					<div className='visualization--line-chart' ref='visualization'></div>
				</div>

        <div className='main-visualization-source' ref='source'>
          <div className='source-content'>
            <div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
            <button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
          </div>
        </div>

        <div className='toolbar--bottom'>
          <div className='container--narrow px2 py1 clearfix'>
            <div className='col col-sm-4 col-sm-offset-8 text--right'>
              <button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
            </div>
          </div>
        </div>

			</div>
		);
	}
}

export default Visualization;
