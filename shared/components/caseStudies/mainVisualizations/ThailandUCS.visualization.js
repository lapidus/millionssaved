
import React from 'react';
import d3 from 'd3';

import d3Tip from 'd3-tip';

class Visualization extends React.Component {

	constructor(props) {
    super(props);

    this.showSource = this.showSource.bind(this);
    this.hideSource = this.hideSource.bind(this);
  }

	componentDidMount() {

		// var el = this.refs.visualization.getDOMNode();
		var el = this.refs.visualization;
		var visualizationContainer = d3.select(el);

		var dataset = [
			// { name: 'Overall', description: '', pre: 0.0664, post: 0.0701, change: 0.054 },
			{ name: 'UNINS', description: 'Uninsured', pre: 5.85, post: 6.06, change: 0.0356, ppChange: 0.21 },
			{ name: 'MWS', description: 'Medical Welfare Scheme', pre: 7.12, post: 7.92, change: 0.114, ppChange: 0.8 },
			{ name: 'Control', description: 'Civil Servants Medical Benefits Scheme (CSMBS) and Social Security Scheme (SSS)', pre: 7.93, post: 7.89, change: -0.005, ppChange: -0.04 }
		];

		var heightScale = d3.scale.linear()
												.domain([0, 0.1])
												.range([0, 200]);

		var tip = d3Tip().attr('class', 'd3-tip').html((d, i) => {
			return `
				<div class='d3-tip-content text--center'>
					<p class='title m0'>${d.toFixed(2)}%</p>
					<p class="m0">${i > 0 ? '2003-2005 post 30 Baht' : '2001 pre 30 Baht'}</p>
				</div>
			`;
		});

		var groups = visualizationContainer.selectAll('.group__bars')
										.data(dataset)
										.each(function(d, i) {
											var _self = d3.select(this);

											var barDataset = [d.pre, d.post];

											var svg = _self.append('svg')
																		 .attr('height', 240)
																		 .attr('width', 200)
																		 .style('margin', '0 auto');

											svg.selectAll('.bar')
												 .data(barDataset)
												 .enter()
												 .append('rect')
												 	.attr('width', 40)
												 	.attr('height', (d) => d * 20)
												 	.attr('x', (d, i) => i > 0 ? 110 : 50)
												 	.attr('y', (d) => 240 - d * 20)
												 	.attr('fill', (d, i) => i > 0 ? '#B89434' : '#204F5A')
												 	.on('mouseover', tip.show)
													.on('mouseout', tip.hide);

											svg.call(tip);

											svg.append('text')
												 .attr('x', 200 - 150)
												 .attr('y', 240 - d.post * 20 - 50)
												 .text((d.change * 100).toFixed(1) + '%')
												 .attr('font-size', 32)
												 .attr('font-weight', 700);

											svg.append('text')
												 .attr('x', 200 - 150)
												 .attr('y', 240 - d.post * 20 - 35)
												 .text(`[${d.ppChange} percentage points]`)
												 .attr('font-size', 10)
												 .attr('fill', '#B89434');

											svg.append('text')
												 .attr('x', 200 - 150)
												 .attr('y', 240 - d.post * 20 - 15)
												 .text(d.change > 0 ? 'increase' : 'reduction')
												 .attr('font-size', 16);

										});

	}

	showSource() {
    var source = this.refs.source;
    source.style.display = 'block';
    Velocity(source, {
      opacity: [ 1, 0 ]
    }, {
      duration: 500
    });
  }

  hideSource() {
    var source = this.refs.source;
    Velocity(source, {
      opacity: [ 0, 1 ]
    }, {
      duration: 500,
      complete: () => {
        source.style.display = 'none';
      }
    });
  }

	componentWillUnmount() {
		d3.select('.d3-tip').remove();
	}

	render() {
		return (
			<div className='main-visualization-container bg--grey mb3'>
				<div className='toolbar--top'>
					<div className='container--narrow px1 px-sm-2 pt2'>
						<div className='mx1 mx-sm-n2 clearfix'>
							<div className='col col-md-8 px1 px-sm-2'>
								<h2 className='headline m0'>Universal Coverage Scheme Improves<br /><span className='text--yellow'>12 Month Inpatient Utilization</span></h2>
							</div>
						</div>
					</div>
				</div>

				<div className='container--narrow p-x-2 p-t-2'>
					<div className='visualization--thailand-ucs clearfix' ref='visualization'>

						{ /* <div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>Overall</p>
								<p></p>
							</div>
						</div> */ }

						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>UNINS</p>
								<p>Uninsured</p>
							</div>
						</div>
						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>MWS</p>
								<p>Medical Welfare Scheme</p>
							</div>
						</div>
						<div className='group'>
							<div className='group__bars'></div>
							<div className='bars-label pt1'>
								<p className='title mt0'>CSMBS/SSS</p>
								<p>Civil Servants Medical Benefits Scheme (CSMBS), Social Security Scheme (SSS)</p>
							</div>
						</div>

					</div>
				</div>

				<div className='main-visualization-source' ref='source'>
					<div className='source-content'>
						<div dangerouslySetInnerHTML={{ __html: this.props.source }}></div>
						<button className='btn bg--yellow px2 py1' onClick={this.hideSource}>Back to visualization</button>
					</div>
				</div>

				<div className='toolbar--bottom'>
					<div className='container--narrow px2 py1 clearfix'>
						<div className='col col-sm-4 col-sm-offset-8 text--right'>
							<button className='btn bg--yellow px1' onClick={this.showSource}>i</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Visualization;
