
import React from 'react';

import GetBook from './caseStudies/utilities/GetBook';


var CircleElement = (props) => (<div className='circle-element mb1'>{props.sign}</div>);

class FindingItem extends React.Component {
	render() {
		return (
			<div className='container--narrow px1 px-sm-2 pt1 pb2 mb2 bg--white clearfix'>
				<div className='col col-md-2 pt1'>
					<CircleElement sign={this.props.sign} />
				</div>
				<div className='col col-md-10'>
					{ this.props.children }
				</div>
			</div>
		);
	}
}

class Feature extends React.Component {
	render() {
		return (
			<div className='col col-md-6 col-lg-6 px1 px-sm-n2 py1'>
				<div className='col col-md-2'>
					<CircleElement sign={this.props.sign} />
				</div>
				<div className='col col-md-10'>
					{ this.props.children }
				</div>
			</div>
		);
	}
}

class Findings extends React.Component {
	render() {
		return (
			<div>
				<header className='banner banner--page'>
					<img src='img/about/findings-cover.jpg' />
					<div className='banner-overlay text--center'>
						<div className='container--narrow y-center clearfix px1 px-sm-2'>
							{ /* <h1 className='display--4 element--0'>Findings</h1>
							<p className='display--1 visible--sm element--1'>Common Features and Key Lessons</p> */ }

							<h1 className='element--0'>
								<span className='subhead text--yellow text--uppercase'>Millions Saved</span><br /><hr className='divider--sm' />
								<span className='display--4'>Findings</span>
							</h1>
							<p className='subhead visible--sm element--1 text--yellow text--uppercase'>Common Features and Key Lessons</p>
						</div>
					</div>
					<div className='img-source'>
						<a href='#' target='_blank'>Photographer — flickr account...</a>
					</div>
				</header>

				<section className='bg--grey pb3'>

					<div className='container--narrow text--center px1 px-sm-2 py3'>
						<p className='lead'>While each case is unique and context-specific, all of the cases have the following four features in common.</p>
					</div>

					<div className='container px1 px-sm-2'>
						<div className='mxn1 mx-sm-n2 clearfix'>
							<Feature sign='1'>
								<p>Wise choices were made about the interventions or tactics to be deployed, based on the best available scientific evidence.</p>
							</Feature>
							<Feature sign='2'>
								<p>Partnerships and coalitions were formed to mobilize needed technical, financial, and political resources, domestically and internationally.</p>
							</Feature>
							<Feature sign='3'>
								<p>Not one, but many political leaders – sometimes across political cycles – sustained efforts over time.</p>
							</Feature>
							<Feature sign='4'>
								<p>The programs used data, results, and evaluation in their particular settings and countries, and parlayed this information to improve health.</p>
							</Feature>
						</div>
					</div>

					<header className='container--narrow text--center px1 px-sm-2 py3'>
						<h1 className='element--0'>
							<span className='subhead text--yellow text--uppercase'>Seven</span><br /><hr className='divider--sm' />
							<span className='display--2'>Key Findings</span>
						</h1>
					</header>

					<FindingItem sign='1'>
						<h2 className='title'>Millions Saved shows that global health works.</h2>
						<p>High profile disease outbreaks, natural disasters, corruption and economic woes sometimes seem to conspire to create an atmosphere of pessimism. But the global health revolution writ large, and the Millions Saved cases in particular, show this pessimism has little place in global health. With the right tactics—reaching the right people at the right time—health can improve rapidly, even in the poorest countries and among the poorest people. Just a few of the programs featured in Millions Saved cases saved more than 18 million years of life that would otherwise have been lost to preventable causes of death and disability. And these huge gains have come at a remarkably low cost.</p>
					</FindingItem>

					<FindingItem sign='2'>
						<h2 className='title'>Focusing on the worst-off yields the biggest health gains.</h2>
						<p>Many cases targeted people who live in poverty or belong to high-risk groups. And programs that were better able to reach the groups most in need achieved larger health impact. This makes intuitive sense: more health progress is possible where baseline conditions are worse. But good targeting comes in many forms. For instance, Brazil’s Programa Saúde da Família allocated more funding to poorer municipalities based on poverty levels, and Kenya’s cash transfer program used geographic and community-based targeting, asking village leaders to identify families eligible for the program.</p>
						<p>Other interventions were universal in scope. For example, the enforcement of Vietnam’s helmet legislation affects the poor and wealthy alike. Even within universal approaches, dedicated efforts are often needed to reach people in the most excluded communities, via targeted outreach, subsidies, and community monitoring.</p>
					</FindingItem>

					<FindingItem sign='3'>
						<h2 className='title'>Governments can do the job; aid helps.</h2>
						<p>In nearly all the cases, governments in low- and middle-income countries have led the hard work of reaching populations in need, making policies, and forming strategies. In South Africa, the post-apartheid government used the Child Support Grant as a central spoke in its strategy to undo the legacy of the past. In Thailand, advocates convinced the government to take on Big Tobacco with far-reaching legislation. Even in countries that some label “failed states,” health authorities have managed to work effectively. Three cases—elimination of polio in Haiti, cash transfers in Pakistan, and vaccination in Africa’s meningitis belt—show that weak governance in general does not preclude effective government-led health-service delivery when the right external support is available.</p>
						<p>Indeed, the partnerships described in the cases show that success results from shared responsibility; all do their part and no one partner foots the bill alone. Most cases feature external co-financing or technical cooperation. Many programs were critically aided by the contributions of global partnerships, bilateral and multilateral aid agencies, and foundations. The private sector also played a role. For example, pharmaceutical companies donated medicines in Botswana and copper companies delivered malaria control programs in Zambia.</p>
					</FindingItem>

					<FindingItem sign='4'>
						<h2 className='title'>Incentives matter for health results.</h2>
						<p>Incentives matter for health—and incentives can take many shapes and forms. For providers, incentives can help motivate greater effort and productivity. They might include the amount of money health workers receive for their services, or steps to monitor performance and promote accountability, to name a few. Similarly, incentives can help motivate individual program beneficiaries to adopt healthier behavior, seek health care services, and adhere to treatment. Paying households and providers in a way that is consistent with desired health outcomes and measuring what matters can make a major difference for health outcomes. However, incentives are powerful, so it is important to ensure that they make sense and do not induce harmful unintended consequences.</p>
					</FindingItem>

					<FindingItem sign='5'>
						<h2 className='title'>What works: efficacy is not the same as effectiveness.</h2>
						<p>In the field of global health, conventional wisdom often suggests that good technologies—those proven to efficacious, to work in a small-scale trial—are enough to get the job done. Historically, the global health community has focused on buying vaccines and medicines for countries that cannot afford them, assuming that those products will make their way to those who need them most. Indeed, the main raison d’être of global partnerships such as the Gavi, the Vaccine Alliance; the Stop TB Fund; UNITAID; and others is to purchase health products, on the implicit assumption that the main barrier to health impact is the lack of efficacious and affordable medicines.</p>
						<p>This lack is certainly part of the problem, but it takes far more than an efficacious and affordable technology to improve health at scale. Efficient delivery, appropriate use, and adherence to treatment directives are equally important ingredients of effectiveness.</p>
						<p>The cases also signal that despite our knowing “what works” in terms of health technology, we still have a lot to learn about how to scale up delivery and uptake. We must carefully evaluate alternative technologies and delivery strategies in different country contexts to figure out the best way to graduate an efficacious technology to effectiveness at scale.</p>
					</FindingItem>

					<FindingItem sign='6'>
						<h2 className='title'>There’s an evaluation revolution, too.</h2>
						<p>Many health programs are judged on their intermediate outputs—the number of children vaccinated or the number of vaccine doses purchased—without a direct assessment of health impact. At the same time, many low- and middle-income countries are seeing rapid improvement in other drivers of health, such as girls’ education, urbanization, and economic growth. Why is this important? Because if we knew that health would have improved even without a health intervention, the money could have been better spent elsewhere.</p>
						<p>Of the 22 new cases, 14 used experimental study designs that allowed for the unambiguous attribution of health impact. Some governments stepped forward to involve themselves in commissioning or carrying out evaluations. In Argentina, South Africa, Thailand, and Mexico, for example, government evaluation agencies have been set up to assure rigorous evaluation methods and the translation of results into policy.</p>
						<p>In some cases, attributable impact is evident even without rigorous evaluation. Zero smallpox cases is zero smallpox cases, and we only need a high-quality disease surveillance system, not an experiment, to understand program results. However, an impact evaluation might still be useful to help us learn about effective immunization delivery strategies. And in countries where other transformations are taking place, it is helpful to understand whether disease trends are most affected by a program or by some other factor.</p>
						<p>Despite the real progress that has been made in the world of impact evaluation, many needed types of data are unavailable. For instance, cost-effectiveness is important to many donors and policymakers who want to know if the health gained is worth the cost of the program—and scarce health dollars. Yet few studies reported empirical estimates of cost-effectiveness; the Millions Saved team had to derive the other estimates from modelling and secondary sources. And some categories of intervention—for example, those against non-communicable diseases—remain woefully under-evaluated.</p>
					</FindingItem>

					<FindingItem sign='7'>
						<h2 className='title'>Evidence requires its own advocacy.</h2>
						<p>Policymakers do not always act on evaluation results, positive or negative. Inertia, often coupled with political considerations, makes it hard to stop something once it starts. Further, policymakers may not even know about failure thanks to publication bias. Less than half of randomized control trials in health care reach publication, and those that do tend to be heavily biased toward statistically significant results that suggest a drug or program was successful.</p>
						<p>In an ideal world, policymakers absorb evaluation results and adjust their programs to enhance effectiveness. In reality, it is not enough to evaluate a program; evidence requires its own advocacy. In some settings, public institutions directly commission the evaluations of public programs and promote action based on the results. There is also a special role aid; <a href='http://www.cgdev.org/sites/default/files/CGD-Policy-Paper-Levine-Savedoff-Future-Aid.pdf' target='_blank'>Ruth Levine and Bill Savedoff have argued</a> that donors are “uniquely suited” to financing evaluations because of the small relative size of donor monies as domestic finance grows, as well as donors’ ambitions of disproportionate influence, sensitivity to being used for illicit purposes, ability to bridge several communities, and aspirational role in advancing public-sector accountability.</p>
					</FindingItem>

				</section>

				<section>
					<GetBook />
				</section>

			</div>
		);
	}
}

export default Findings;
