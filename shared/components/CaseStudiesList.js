
import React from 'react';
import { connect } from 'react-redux';
import { fetchCaseStudiesIfNeeded, changeListStyle, changeRegionFilter, changeApproachFilter } from '../actions/caseStudyActions';
import { Link } from 'react-router';

import lodashFilter from 'lodash/collection/filter';
import lodashSortBy from 'lodash/collection/sortBy';
import classNames from 'classnames';

import Map from './OverviewMap';

import Select from 'react-select';


// Sample if statement in pure jsx: {(() => { return caseStudy.caseType === 1 ? (<div className='extended-badge'>*</div>) : '' }())}
var jsxIf = (condition, option1, option2) => condition ? option1 : (option2 || '');


class ListItem extends React.Component {
	render() {
		var caseStudy = this.props.caseStudy;
		var displayStyles = {
			'grid': 'col col-sm-6 col-lg-4 px1 py1 grid-item',
			'list': 'col col-md-10 col-lg-8 px1 pt1 mb1 list-item',
			'map': ''
		};
		var displayStyle = displayStyles[this.props.displayStyle];

		var imageUrl;
		var imgNameArray;

		if(caseStudy.image && caseStudy.image.filename) {
			imgNameArray = caseStudy.image.filename.split('.jpg');
			imageUrl = `//millionssaved.s3-us-west-2.amazonaws.com/md/${imgNameArray[0]}_md.jpg`;
		}
		else if(caseStudy.template || caseStudy.template === 0) {
			imageUrl = `/img/case-studies/medium/case-study-${caseStudy.template}.jpg`
		}
		else {
			imageUrl = '';
		}

		return (
			<article className={displayStyle}>
				<div className='item__inner'>
					{jsxIf(imageUrl,
						<Link to={'/case-studies/' + caseStudy.slug} tabIndex='-1'>
							<img src={imageUrl} />
						</Link>
					)}

					{jsxIf(caseStudy.caseType === 1,
						<div className='extended-badge'>*</div>
					)}

					<div className='item__content pb1'>

						{jsxIf(this.props.displayStyle === 'grid',
							<div className='px2 py1 approach'>{caseStudy.approach}</div>
						)}

						<p className='px2'>{caseStudy.preTitle}</p>
						<hr className='divider--sm m0' style={{width: '100%'}}/>
						<h1 className='headline px2'>
							<Link to={'/case-studies/' + caseStudy.slug}>{caseStudy.title}</Link>
						</h1>
					</div>
				</div>
			</article>
		);
	}
}

class CaseListing extends React.Component {
	render() {
		var cases = lodashFilter(this.props.caseStudies, (n) => {
			if(this.props.regionFilter === 'all') {
				return n;
			}
			return n.region == this.props.regionFilter;
		});
		cases = lodashFilter(cases, (n) => {
			if(this.props.approachFilter === 'all') {
				return n;
			}
			return n.approach == this.props.approachFilter;
		});

		cases = lodashSortBy(cases, (n) => {
			return -n.caseType;
		});

		return (
			<div className='container px1 pt1'>
				<div className='case-study-list__items'>
					<div className='mxn1 clearfix'>
						{ cases.map((caseStudy) => {
								return (<ListItem caseStudy={caseStudy} displayStyle={this.props.listDisplayStyle} key={caseStudy._id} />);
						})}
					</div>
				</div>
			</div>
		);
	}
}

class CaseStudiesList extends React.Component {
	constructor() {
		super();

		this.handleRegionChange = this.handleRegionChange.bind(this);
		this.handleApproachChange = this.handleApproachChange.bind(this);
		this.handleListViewStyleChange = this.handleListViewStyleChange.bind(this);
	}
	handleRegionChange(region) {
		this.props.dispatch(changeRegionFilter(region));
	}
	handleApproachChange(approach) {
		this.props.dispatch(changeApproachFilter(approach));
	}

	handleListViewStyleChange(type) {
		this.props.navigateToSection('main');
		this.props.dispatch(changeListStyle(type));
	}

	render() {

		var regions = [
			{ value: 'all', label: 'All Regions' },
			{ value: 'Africa', label: 'Africa' },
			{ value: 'Latin America', label: 'Latin America' },
			{ value: 'South-East Asia', label: 'South-East Asia' }
		];

		var approaches = [
			{ value: 'all', label: 'All Approaches' },
			{ value: 'Medical Rollout and Technologies', label: 'Medical Rollout and Technologies' },
			{ value: 'Expanding Access to Health Services', label: 'Expanding Access to Health Services' },
			{ value: 'Cash Transfers to Improve Health', label: 'Cash Transfers to Improve Health' },
			{ value: 'Behavior Change to Decrease Risk', label: 'Behavior Change to Decrease Risk' }
		];

		return (
			<div>
				<div className='container px1 pt3' ref='scrollTarget'>

					<div className='case-study-list__filters'>
						<div className='mxn1 clearfix'>
							<div className='col col-md-4 px1 mb1'>
								<Select searchable={false} clearable={false} name='something' value={this.props.regionFilter} options={regions} onChange={this.handleRegionChange} placeholder='Filter by Region' />
							</div>
							<div className='col col-md-4 px1 mb1'>
								<Select searchable={false} clearable={false} name='something' value={this.props.approachFilter} options={approaches} onChange={this.handleApproachChange} placeholder='Filter by Approach' />
							</div>
							<div className='col col-md-4 px1 mb1 visible--sm'>
								<div className={classNames('btn', 'bg--yellow', 'col-xs-4', 'py1', {'active': this.props.listDisplayStyle == 'grid'})} onClick={() => this.handleListViewStyleChange('grid') }>Grid</div>
								<div className={classNames('btn', 'bg--yellow', 'col-xs-4', 'py1', {'active': this.props.listDisplayStyle == 'map' })} onClick={() => this.handleListViewStyleChange('map') }>Map</div>
								<div className={classNames('btn', 'bg--yellow', 'col-xs-4', 'py1', {'active': this.props.listDisplayStyle == 'list'})} onClick={() => this.handleListViewStyleChange('list') }>List</div>
							</div>
						</div>
					</div>

				</div>

				{jsxIf(this.props.listDisplayStyle === 'map',

					<Map caseStudies={this.props.caseStudies}
							 approachFilter={this.props.approachFilter}
							 regionFilter={this.props.regionFilter} />,

					<CaseListing listDisplayStyle={this.props.listDisplayStyle}
											 approachFilter={this.props.approachFilter}
											 regionFilter={this.props.regionFilter}
											 caseStudies={this.props.caseStudies} />
				)}

			</div>
		);
	}
}

CaseStudiesList.needs = [ fetchCaseStudiesIfNeeded ];

function mapStateToProps(state) {
	return {
		caseStudies: state.caseStudiesReducer.caseStudies,
		listDisplayStyle: state.caseStudiesReducer.listDisplayStyle,
		regionFilter: state.caseStudiesReducer.regionFilter,
		approachFilter: state.caseStudiesReducer.approachFilter,
		dispatch: state.caseStudiesReducer.dispatch
	};
}

export default connect(mapStateToProps)(CaseStudiesList);
