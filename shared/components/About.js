
import React from 'react';
import { Link } from 'react-router';

class About extends React.Component {
	render() {
		return (
			<div>
				<header className='banner banner--page'>
					<img src='/img/about/ms-home-about.jpg' />
					<div className='banner-overlay text--center'>
						<div className='container--narrow y-center clearfix px1 px-sm-2'>
							<h1 className='element--0'>
								<span className='subhead text--yellow text--uppercase'>What is</span><br /><hr className='divider--sm' />
								<span className='display--4'>Millions Saved?</span>
							</h1>
							<p className='subhead visible--sm element--1 text--yellow text--uppercase'>The Project, Cases and Team</p>
						</div>
					</div>
					<div className='img-source'>
						<a href='#' target='_blank'>Photographer — flickr account...</a>
					</div>
				</header>

				<div className='bg--yellow about-submenu'>
					<div className='container--narrow clearfix text--center px1 px-sm-2'>
						<div className='mxn1'>
							<Link to='/about' className='btn px1 py1'>Background</Link>
							<Link to='/about/cases' className='btn px1 py1'>Cases and Selection Process</Link>
							<Link to='/about/team' className='btn px1 py1'>The Team</Link>
						</div>
					</div>
				</div>

				<div id='subview'>
					<div>{this.props.children}</div>
				</div>

			</div>
		);
	}
}

export default About;
