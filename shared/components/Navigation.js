
import React from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router';
import Icon from '../lib/icons';

import colors from './caseStudies/utilities/colors';

import { fetchCaseStudiesIfNeeded } from '../actions/caseStudyActions';

class NavItem extends React.Component {
	render() {

		var {slug, name, itemStyle} = this.props;

		return (
			<li className={itemStyle}>
				<Link to={slug} activeClassName='active' onClick={this.props.forceCloseNavigation}>
					<span>{name}</span>
				</Link>
			</li>
		);
	}
}

class NavDropdown extends React.Component {
	constructor() {
		super();

		this.state = {
			open: false
		};

		this.openDropdown = this.openDropdown.bind(this);
		this.closeDropdown = this.closeDropdown.bind(this);
		this.catchClick = this.catchClick.bind(this);
	}

	catchClick(e) {
		e.preventDefault();
	}

	openDropdown() {
		this.setState({
			open: true
		});
	}

	closeDropdown() {
		this.props.forceCloseNavigation();
		this.setState({
			open: false
		});
	}

	render() {

		var {slug, name, dropdown, itemStyle} = this.props;

		var openDropdown = this.openDropdown;
		var closeDropdown = this.closeDropdown;
		var itemActiveStyle = this.state.open && dropdown ? {background: colors.teal600} : {};

		return (
			<li className={itemStyle} style={itemActiveStyle}>

				<Link to={slug} activeClassName='active' onFocus={openDropdown} onBlur={closeDropdown} onClick={this.catchClick}>
					<span>{name}</span>
				</Link>

				<div className={this.state.open ? 'dropdown dropdown--open' : 'dropdown'}>
					<div className='container px1 py3'>
						<ul className={this.state.open ? 'o1 mxn1 clearfix' : 'o0 mxn1 clearfix'}>
							{dropdown.map((item, key) => {
								if(item.caseType === 0) return;
								return (
									<li key={key} className='col col-md-4 px1 mb1'>
										<Link to={`/case-studies/${item.slug}`} onFocus={openDropdown} onBlur={closeDropdown} onClick={closeDropdown}>
											{item.title}
										</Link>
									</li>
								);
							})}
						</ul>
					</div>
				</div>

			</li>
		);
	}
}

class Navigation extends React.Component {
	render() {

		var itemStyle = 'main-navigation__item px1';

		var navItems = [
			{
				slug: '',
				name: 'Case Studies',
				dropdown: this.props.caseStudies,
				itemStyle: itemStyle + ' main-navigation__item--dropdown'
			},
			{
				slug: '/findings',
				name: 'Findings',
				itemStyle: itemStyle
			},
			{
				slug: '/frequently-asked-questions',
				name: 'FAQ',
				itemStyle: itemStyle
			},
			{
				slug: '/media',
				name: 'Media',
				itemStyle: itemStyle
			},
			{
				slug: '/about',
				name: 'About',
				itemStyle: itemStyle
			}
		];

		return (
			<div className='main-navigation'>
				<div className='main-navigation__toggle'>
					<button className='btn bg--yellow px1 py1' onClick={this.props.toggleNavigation}>
						<Icon name='nav' color='#333333' /><span style={{marginLeft:'8px'}} className='visible--sm-inline'>MENU</span>
					</button>
				</div>
				<nav className={this.props.navigationActive ? 'nav-active' : ''}>
					<ul>

						{navItems.map((navItem, key) => {

							if(!navItem.dropdown)
								return <NavItem {...navItem} toggleNavigation={this.props.toggleNavigation} forceCloseNavigation={this.props.forceCloseNavigation} key={key}/>;

							else
								return <NavDropdown {...navItem} toggleNavigation={this.props.toggleNavigation} forceCloseNavigation={this.props.forceCloseNavigation} key={key}/>;

						})}

					</ul>
				</nav>
			</div>
		);
	}
}

Navigation.needs = [ fetchCaseStudiesIfNeeded ];

function mapStateToProps(state) {
	return {
		caseStudies: state.caseStudiesReducer.caseStudies,
		dispatch: state.caseStudiesReducer.dispatch
	};
}

export default connect(mapStateToProps)(Navigation);
