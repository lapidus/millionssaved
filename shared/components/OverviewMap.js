
import React from 'react';
import ReactDOM from 'react-dom';
import d3 from 'd3';
import d3Tip from 'd3-tip';
import topojson from 'topojson';

import colors from './caseStudies/utilities/colors';

import lodashFilter from 'lodash/collection/filter';
import { connect } from 'react-redux';

import { getMap } from '../actions/mapActions';

class OverviewMap extends React.Component {
	constructor() {
		super();

		this.filterCases = this.filterCases.bind(this);
		this.updateMap = this.updateMap.bind(this);
		this.initMap = this.initMap.bind(this);
	}

	filterCases() {
		var approachFilter = this.props.approachFilter;
		var regionFilter = this.props.regionFilter;

		var cases = lodashFilter(this.props.caseStudies, (n) => {
			if(regionFilter === 'all') {
				return n;
			}
			return n.region == regionFilter;
		});
		cases = lodashFilter(cases, (n) => {
			if(approachFilter === 'all') {
				return n;
			}
			return n.approach == approachFilter;
		});

		return cases;
	}

	componentDidMount() {
		if(this.props.worldMap.arcs) {
			this.initMap();
		}
		else {
			this.props.dispatch(getMap());
		}
	}

	initMap() {
		var el = ReactDOM.findDOMNode(this);

		var mapContainer = d3.select(el);

		var width = window.outerWidth,
				height = 660;

		var minColor = '#00adee',
				maxColor = '#aabb00';

		var cases = this.filterCases();

		var colorScale = d3.scale.linear()
											 .domain([0, 2000])
											 .range([minColor, maxColor]);

		this.projection = d3.geo.mercator()
											 .scale(220)
											 .rotate([-10,0,0])
											 .translate([width / 2, height / 1.7]);

		var path = d3.geo.path().projection(this.projection);

		var svg = mapContainer.append('svg')
								.attr('width', '100%')
								.attr('height', height)
								.attr('viewBox', '0 0 ' + width + ' ' + height)
								.append('g');

		var startCoords;
		var endCoords;

		var drag = d3.behavior.drag()
								 .on('dragstart', function(d) {
										startCoords = endCoords ? [d3.event.sourceEvent.layerX - endCoords[0], d3.event.sourceEvent.layerY - endCoords[1]] : [d3.event.sourceEvent.layerX, d3.event.sourceEvent.layerY];
								 })
								 .on('drag', function(d) {
										d3.select(this)
											.style('transform', `translate(${-(startCoords[0] - d3.event.x)}px,${-(startCoords[1] - d3.event.y)}px)`);
										endCoords = [-(startCoords[0] - d3.event.x), -(startCoords[1] - d3.event.y)];
								 });

		this.map = svg.insert('g')
									 .attr('class', 'world')
									 .call(drag);

		this.map.append('rect')
			 .attr('class', 'overlay')
			 .attr('width', width)
			 .attr('height', height);

		var topojsonShapes = topojson.feature(this.props.worldMap, this.props.worldMap.objects.countries).features;

		var countryShapes = this.map.selectAll('.shape')
			 .data(topojsonShapes)
			 .enter()
			 .append('path')
			 .attr('class', () => {
			 	return 'fill--grey-light shape';
			 })
			 .attr('d', path);
			 // .style('fill', '#e1e2e3');

		this.tip = d3Tip().attr('class', 'd3-tip').html((d) => {
			return `
				<div class="d3-tip-img">
					<img src='/img/case-studies/medium/case-study-${d.template}.jpg' />
				</div>
				<div class='d3-tip-content'>
					<a href='/case-studies/${d.slug}'>${d.title}</a>
				</div>
			`;
		});

		this.map.call(this.tip);

		var _self = this;

		var markers = this.map.selectAll('.marker')
				.data(cases)
				.enter()
				.append('circle')
				.attr('class', 'marker')
				.attr('cx', (d) => {
					return d.location ? this.projection([d.location.lon, d.location.lat])[0] : 0;
				})
				.attr('cy', (d) => {
					return d.location ? this.projection([d.location.lon, d.location.lat])[1] : 0;
				})
				// .attr('r', 6)
				.attr('r', (d) => d.caseType === 1 ? 8 : 6)
				.style('transform-origin', '50% 50%')
				// .style('fill', '#204F5A')
				.style('fill', colors.teal500)
				.on('mouseover', function(d) {
					d3.select(this)
						.transition()
						// .style('fill', '#B89434')
						.style('fill', colors.yellow500)
						.attr('r', (d) => d.caseType === 1 ? 14 : 12)
						.duration(300);
					_self.tip.show(d);
				})
				.on('mouseout', function() {
					d3.select(this)
						.transition()
						// .style('fill', '#204F5A')
						.style('fill', colors.teal500)
						.attr('r', (d) => d.caseType === 1 ? 8 : 6)
						.duration(300);
					_self.tip.hide();
				})
				.on('click', function(d) {
					// _self.tip.hide();
					d3.select(this)
						.transition()
						.attr('r', 100)
						.style('opacity', 0)
						.duration(800);
					setTimeout(function() {
						historyHack.pushState(null, `/case-studies/${d.slug}`);
					}, 300);
				});

	}

	updateMap() {
		var cases = this.filterCases();
		this.map.selectAll('.marker').remove();

		var _self = this;

		this.map.selectAll('.marker')
				.data(cases)
				.enter()
				.append('circle')
				.attr('class', 'marker')
				.attr('cx', (d) => {
					return d.location ? this.projection([d.location.lon, d.location.lat])[0] : 0;
				})
				.attr('cy', (d) => {
					return d.location ? this.projection([d.location.lon, d.location.lat])[1] : 0;
				})
				.attr('r', (d) => d.caseType === 1 ? 8 : 6)
				.style('transform-origin', '50% 50%')
				// .style('fill', '#204F5A')
				.style('fill', colors.teal500)
				.on('mouseover', function(d) {
					d3.select(this)
						.transition()
						// .style('fill', '#B89434')
						.style('fill', colors.yellow500)
						.attr('r', (d) => d.caseType === 1 ? 14 : 12)
						.duration(300);
					_self.tip.show(d);
				})
				.on('mouseout', function() {
					d3.select(this)
						.transition()
						// .style('fill', '#204F5A')
						.style('fill', colors.teal500)
						.attr('r', (d) => d.caseType === 1 ? 8 : 6)
						.duration(300);
					_self.tip.hide();
				})
				.on('click', function(d) {
					// _self.tip.hide();
					d3.select(this)
						.transition()
						.attr('r', 100)
						.style('opacity', 0)
						.duration(800);
					setTimeout(function() {
						historyHack.pushState(null, `/case-studies/${d.slug}`);
					}, 300);
				});
	}

	componentDidUpdate(prevProps) {
		if(!this.props.worldMap.arcs) {
			return;
		}
		if(!prevProps.worldMap.arcs && this.props.worldMap.arcs) {
			this.initMap();
		}
		else {
			this.updateMap();
		}
	}

	componentWillUnmount() {
		var tip = d3.select('.d3-tip');
		if(tip) {
			tip.remove();
		}
	}

	render() {
		return (
			<div className='world-map'></div>
		);
	}
}

function mapStateToProps(state) {
	return {
		isFetching: state.mapReducer.isFetching,
		worldMap: state.mapReducer.worldMap,
		dispatch: state.caseStudiesReducer.dispatch
	};
}

export default connect(mapStateToProps)(OverviewMap);
