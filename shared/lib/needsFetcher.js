
export default function fetchNeededData(dispatch, components, params, fetcher) {
	const needs = components.reduce((prev, current) => {
		return (current.needs || [])
			.concat((current.WrappedComponent ? current.WrappedComponent.needs : []) || [])
			.concat(prev);
	}, []);
	const promises = needs.map((need) => {
		return dispatch(need(params, fetcher));
	});
	// console.timeEnd('Running fetch');

	return Promise.all(promises);
}
