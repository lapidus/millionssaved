
export default function checkForNestedNeeds(component) {
	// console.log('Checking for nested needs');
	var needs = component.needs || [];
	needs = needs.concat(component.WrappedComponent ? component.WrappedComponent.needs || [] : []);
	return needs;
}
