
import Fetcher from 'fetchr';

export const REQUEST_POSTS = 'REQUEST_POSTS';
export const RECEIVE_POSTS = 'RECEIVE_POSTS';

function requestPosts() {
  return {
    type: REQUEST_POSTS
  };
}

function receivePosts(posts) {
  return {
    type: RECEIVE_POSTS,
    posts: posts,
    receivedAt: Date.now()
  };
}

function fetchPosts(fetcher) {
  return (dispatch) => {
    dispatch(requestPosts());

    var promise = new Promise((resolve, reject) => {

      fetcher
        .read('post-service')
        .end((err, data) => {

          if(err) {
            reject();
          }

          else {
            dispatch(receivePosts(data));
            resolve(data);
          }

        });

    });

    return promise;
  }
}

function shouldFetchPosts(state) {
  const posts = state.postReducer.posts;
  if (posts.length === 0) {
    return true;
  } else if (posts.isFetching) {
    return false;
  } else {
    return posts.didInvalidate;
  }
}

export function fetchPostsIfNeeded(params, fetcher) {
  return (dispatch, getState) => {
    if (shouldFetchPosts(getState())) {
      return dispatch(fetchPosts(fetcher));
    }
  };
}
