
import d3 from 'd3';

export const REQUEST_MAP = 'REQUEST_MAP';
export const RECEIVE_MAP = 'RECEIVE_MAP';

function requestMap() {
  return {
    type: REQUEST_MAP
  };
}

function receiveMap(worldMap) {
  return {
    type: RECEIVE_MAP,
    worldMap: worldMap,
    receivedAt: Date.now()
  };
}

function fetchMap() {
  return (dispatch) => {
    dispatch(requestMap());

    var promise = new Promise((resolve, reject) => {
      var url = window.location.origin + '/files/world-50m.json';

      d3.json(url, function(err, world) {
        if(err) {
          reject(err);
        }

        dispatch(receiveMap(world));
        resolve(world);
      });

    });

    return promise;
  }
}

export function getMap() {
  return (dispatch, getState) => {
    if(shouldFetchMap(getState())) {
      return dispatch(fetchMap());
    }
  };
}

function shouldFetchMap(state) {
  var condition1 = state.mapReducer.isFetching ? false : true;
  var condition2 = state.mapReducer.worldMap.arcs ? false : true;

  return condition1 && condition2;
}
