'use strict';

import keystone from 'keystone';
import knox from 'knox';
import async from 'async';

var gm = require('gm').subClass({imageMagick: true});

//
// Specify copies of the original image to be saved, when an image is uploaded
//
export const copies = [
  {
    name: 'xs',
    prefix: '/xs/',
    suffix: 'xs',
    width: 160,
    height: 160
  },
  {
    name: 'sm',
    prefix: '/sm/',
    suffix: 'sm',
    width: 240,
    height: 170
  },
  {
    name: 'md',
    prefix: '/md/',
    suffix: 'md',
    width: 800,
    height: 560
  },
  {
    name: 'lg',
    prefix: '/lg/',
    suffix: 'lg',
    width: 1440,
    height: 800
  }
];

export function deleteImage(opts) {
  var file = opts.file;
  var next = opts.next;

  if(!file.filename) {
    next();
    return;
  }

  var filenameArray = file.filename.split('.');

  var s3config = keystone.get('s3 config'),
      client = knox.createClient(s3config);

      console.log(`Want to delete: /${file.filename}`);

  client.del(`/${file.filename}`).on('response', function(res) {
    console.log('Deleted main image!');
    next();
  }).end();

  function deleteResponse(res) {
    console.log('Deleted copies!');
  }

  for (var i = 0, len = copies.length; i < len; i++) {
    console.log(`Deleting copy: ${copies[i].prefix}${filenameArray[0]}_${copies[i].suffix}.${filenameArray[1]}`);
    client.del(`${copies[i].prefix}${filenameArray[0]}_${copies[i].suffix}.${filenameArray[1]}`).on('response', deleteResponse).end();
  };

}

export function uploadImage(opts) {

  var item = opts.item,
      file = opts.file,
      next = opts.next;

  function upload(i, callback) {

    var uploadsPrefix  = copies[i].prefix,
        imageSuffix    = copies[i].suffix;

    var s3config = keystone.get('s3 config'),
        s3ItemPath = `https://s3-${s3config.region}.amazonaws.com/${s3config.bucket + uploadsPrefix + file.name}`,
        client = knox.createClient(s3config);

    gm(file.path)
      .resize(copies[i].width, copies[i].height, '^')
      .density(72, 72)
      .gravity('Center')
      .crop(copies[i].width, copies[i].height)
      .quality(80)
      .interlace('Line')
      .stream(function(err, stdout, stderr) {
        if(err) callback(err);
        
        var i = [];
        
        stdout.on('data', function (data) {
          i.push(data);
        });

        stdout.on('close', function() {

          var image = Buffer.concat(i);

          var headers = {
            'Content-Length': image.length,
            'Content-Type': file.mimetype,
            'Cache-Control': 'max-age=86400',
            'x-amz-acl': 'public-read'
          };

          var dotIndex = file.name.indexOf('.');
          var name = `${file.name.slice(0, dotIndex)}_${imageSuffix}.${file.extension}`;

          var req = client.put(uploadsPrefix + name, headers);

          req.on('response', function(res) {
            item[`img_${imageSuffix}`] = `//s3-${s3config.region}.amazonaws.com/${s3config.bucket + uploadsPrefix + name}`;
            callback(null, image);
          });

          req.end(image);
        });

      });
  }

  // ========== ========== ========== ========== ==========

  var calls = {};

  copies.forEach(function(copy, i) {
    calls[copy.name] = function(callback) {
      upload(i, callback);
    };
  });

  async.parallel(
    calls,
    function(err, results) {
      if(err) {
        console.log('There was an error when uploading the image', err);
      }
      next();
    }
  );

}
