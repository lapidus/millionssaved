
import keystone from 'keystone';
var CaseStudy = keystone.list('CaseStudy');

const miniVisualizationService = {
	name: 'mini-visualization-service',
	read(req, resource, params, config, callback) {
		CaseStudy
			.model
			.findOne({ slug: params.slug })
			.select(`
				background.visualizationOne.html
				background.visualizationTwo.html
				background.visualizationThree.html
				background.visualizationFour.html
				background.visualizationFive.html
				background.visualizationSix.html
				programRollout.visualizationOne.html
				programRollout.visualizationTwo.html
				programRollout.visualizationThree.html
				programRollout.visualizationFour.html
				programRollout.visualizationFive.html
				programRollout.visualizationSix.html
				programRollout.visualizationSeven.html
				programRollout.visualizationEight.html
				programRollout.visualizationNine.html
				impact.visualizationOne.html
				impact.visualizationTwo.html
				impact.visualizationThree.html
				impact.visualizationFour.html
				impact.visualizationFive.html
				impact.visualizationSix.html
				cost.visualizationOne.html
				cost.visualizationTwo.html
				cost.visualizationThree.html
				cost.visualizationFour.html
				cost.visualizationFive.html
				cost.visualizationSix.html
				reasonsForSuccess.visualizationOne.html
				reasonsForSuccess.visualizationTwo.html
				reasonsForSuccess.visualizationThree.html
				reasonsForSuccess.visualizationFour.html
				reasonsForSuccess.visualizationFive.html
				reasonsForSuccess.visualizationSix.html
				implications.visualizationOne.html
				implications.visualizationTwo.html
				implications.visualizationThree.html
				implications.visualizationFour.html
				implications.visualizationFive.html
				implications.visualizationSix.html
			`)
			.exec()
			.then((miniVisualizations) => {
				callback(null, miniVisualizations);
			}, (err) => {
				callback(err);
			});
	}
}
export default miniVisualizationService;
