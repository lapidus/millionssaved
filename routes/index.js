
import Fetcher  from 'fetchr';
import React    from 'react';
import ReactDOM from 'react-dom/server';
// import ReactDOMStream from 'react-dom-stream/server';

import createLocation from 'history/lib/createLocation'
import { RoutingContext, match } from 'react-router'
import routes   from '../shared/routes';

import { Provider }    from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk           from 'redux-thunk';

import caseStudiesReducer     from '../shared/reducers/caseStudiesReducer';
import kenyaVisualizationReducer from '../shared/reducers/kenyaVisualizationReducer';
import menAfriVacVisualizationReducer from '../shared/reducers/menAfriVacVisualizationReducer';
import mapReducer from '../shared/reducers/mapReducer';
import mediaItemsReducer from '../shared/reducers/mediaItemsReducer';

import caseStudyService from '../services/caseStudyService';
import miniVisualizationService from '../services/miniVisualizationService';
import mediaItemService from '../services/mediaItemService';
import fetchNeededData from '../shared/lib/needsFetcher';

import keystone from 'keystone';

var importRoutes = keystone.importer(__dirname);

import {getTemplateCache, setTemplateCache} from './templateCache';

// Setup Route Bindings
export default function(server) {

  // Fetcher.registerService(postService);
  Fetcher.registerService(caseStudyService);
  Fetcher.registerService(miniVisualizationService);
  Fetcher.registerService(mediaItemService);
  server.use('/api', Fetcher.middleware());

  // Secure site during development — Remove this for production...
  server.use((req, res, next) => {
    if(!req.user) {
      req.flash('error', 'Please sign in to access this page.');
      res.redirect('/keystone/signin');
    }
    else {
      next();
    }
  });

  server.use((req, res) => {

    var template = getTemplateCache(req.path);

    if(template) {
      res.send(template.html);
    }
    else {
      let location = createLocation(req.url)
      const store = applyMiddleware(thunk)(createStore)(combineReducers({caseStudiesReducer, kenyaVisualizationReducer, menAfriVacVisualizationReducer, mapReducer, mediaItemsReducer}));

      var fetcher = new Fetcher({
        xhrPath: '/api',
        req: req
      });

      match({ routes, location }, (error, redirectLocation, renderProps) => {
        if (redirectLocation) {
          res.redirect(301, redirectLocation.pathname + redirectLocation.search);
        }
        else if (error) {
          res.send(500, error.message);
        }
        else if (renderProps == null) {
          res.send(404, 'Not found');
        }
        else {
          fetchNeededData(store.dispatch, renderProps.components, renderProps.params, fetcher)
            .then(() => {

              var renderedView = renderView(renderProps);

              setTemplateCache(req.path, renderedView);

              res.send(renderedView);

              // res.send(renderView(renderProps));
              // renderView(renderProps, res);
            })
            .catch((err) => {
              res.send(err.message);
            });
        }
      });

      function renderView(renderProps) {
        const InitialView = (
          <Provider store={store}>
            {() =>
              <RoutingContext {...renderProps} />
            }
          </Provider>
        );

        const componentHTML = ReactDOM.renderToString(InitialView);
        const initialState = store.getState();

        var scriptPath = '/js/client.js';
        var title = 'Millions Saved';

        // <script text="type/javascript">
        //   document.getElementsByTagName('html')[0].classList.remove('no-js');
        // </script>

        // <script src="/js/lib/ScrollMagic.min.js"></script>
        // <script src="/js/lib/velocity.min.js"></script>
        // <script src="/js/lib/animation.velocity.min.js"></script>

        const HTML = `
          <!DOCTYPE html>
          <html>
            <head>
              <meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <title>${title}</title>
              <link rel="stylesheet" href="/styles/main.css">
            </head>
            <body id="body">
              <div id="view-wrapper" class="view-wrapper">
                <div id="react-view">${componentHTML}</div>
              </div>
              <div id="transition-overlay" class="transition-overlay"><div id="loader" class="loader"><div class="cube"></div><p>Loading</p></div></div>
              <script>
                window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
              </script>
              <script src="/js/lib/vendor.js" async></script>
              <script src="${scriptPath}" async></script>
            </body>
          </html>`;

        return HTML;
      }
    } // else ends here...

  });

}
